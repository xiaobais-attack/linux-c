class MinStack {
public:
    MinStack() {
        stack<int> _min_st;
    }
    
    void push(int val) {
        _st.push(val);
        if (_min_st.size() == 0 || val <= _min_st.top())
        {
            _min_st.push(val);
        }

    }
    
    void pop() {
        int t = _st.top();
        _st.pop();
        if (t == _min_st.top())
        {
            _min_st.pop();
        }

    }
    
    int top() {
        return _st.top();

    }
    
    int getMin() {
        return _min_st.top();

    }
private:
    stack<int> _st;
    stack<int> _min_st;
};



class Solution {
public:
    bool IsPopOrder(vector<int> pushV,vector<int> popV) {
        stack<int> st;
        int cur_push = 0;
        int cur_pop = 0;
        while (cur_pop < popV.size())
        {
            while (st.size() == 0 || st.top() != popV[cur_pop])
            {
                if (cur_pop >= popV.size())
                {
                    break;
                }
                st.push(pushV[cur_push]);
                cur_push++;
            }
            while (st.top() == popV[cur_pop])
            {
                if (cur_pop >= popV.size())
                {
                    break;
                }
                st.pop();
                cur_pop++;
                if (st.size() == 0)
                {
                    break;
                }
            }
        }
        if (st.size() == 0)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }
};
