class Solution {
public:
    vector<int> partitionLabels(string s) {
        int v[26];
        for (int i = 0; i < s.size(); i++) {
            v[s[i] - 'a'] = i;
        }

        int start = 0, end = 0;
        vector<int> result;
        for (int i = 0; i < s.size(); i++) {
            end = max(end, v[s[i] - 'a']);
            if (end == i) {
                result.push_back(end - start + 1);
                start = end + 1;
            }
        }
        return result;
    }
};
