#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define COL 3
#define ROW 3

void Menu();
void InitBoard(char board[ROW][COL], size_t row, size_t col);
void DisplayBoard(char board[ROW][COL], size_t row, size_t col);
void PlayerMove(char board[ROW][COL], size_t row, size_t col);
void ComputerMove(char board[ROW][COL], size_t row, size_t col);
char IsWin(char board[ROW][COL], size_t row, size_t col);
size_t Celebrate(char board[ROW][COL], size_t row, size_t col);
