#pragma once 
#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <string>
#include <vector>
// 工具类
void SetNonBlock(int sock){
    int fl = fcntl(sock, F_GETFL);
    if (fl < 0){
        std::cerr << "fcntl failed" << std::endl;
        return;
    }
    fcntl(sock, F_SETFL, fl | O_NONBLOCK);
}

// 分包 只会读取完整报文 --解决粘包问题
void SplitSegment(std::string &inbuffer, std::vector<std::string>* tokens, std::string sep){
    while (true){
        std::cout << "inbuffer: " << inbuffer << std::endl;
        auto pos = inbuffer.find(sep);
        if (pos == std::string::npos) break;
        std::string sub = inbuffer.substr(0, pos);
        tokens->push_back(sub);
        inbuffer.erase(0, pos + sep.size());
    }
}

// 和业务强相关
 bool Deserialize(const std::string &segment, std::string *out1, std::string *out2){
    auto pos = segment.find("+");
    if (pos == std::string::npos) return false;
    *out1 = segment.substr(0, pos);
    *out2 = segment.substr(pos + 1); 
    return true;
 }    