#include "BSTR.h"

void BSTRTest1()
{
  BSTree<int> t;
  int arr[] = {1,5,3,6,2,7,4};
  for (auto e : arr)
  {
    t.InsertR(e);
  }
  t.InOrderR();
  t.EraseR(5);
  t.InOrderR();
}
int main()
{
  BSTRTest1();
  return 0;
}
