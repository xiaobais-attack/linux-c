#pragma once 
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

class Task{
public:
    Task() : sock(-1){};
    Task(int _sock) : sock(_sock){}
    void ProcessOn(){
        // while (true){
            char buffer[1024];
            memset(buffer, 0, sizeof(buffer));
            ssize_t s = read(sock, buffer, sizeof(buffer) - 1);
            if (s > 0){
                //将获取的内容当作字符串
                buffer[s] = 0;
                std::cout << "client# " << buffer << std::endl;
                std::string echo_string = ">>>server<<<";
                echo_string += buffer;
                echo_string += "...";
                write(sock, echo_string.c_str(), echo_string.size());
            }
            else if (s == 0){
                std::cout << "client quit ..." << std::endl;
                //break;
            }
            else {
                std::cerr << "read errno" << errno << std::endl;
                //break;
            }
        //}
        close(sock);
    }    
    void operator()(){
        ProcessOn();
    }
private:
    int sock;
};