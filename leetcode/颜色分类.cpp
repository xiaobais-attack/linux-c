class Solution {
public:
    void sortColors(vector<int>& nums) {
        int r = 0, w = 0;
        for (int i = 0; i < nums.size(); i++) {
            if (nums[i] == 0) {
                if (w > r) {
                    swap(nums[r], nums[i]);
                    swap(nums[i], nums[w]);
                } 
                else {
                    swap(nums[r], nums[i]);
                }
                r++;
                w++;

            } else if (nums[i] == 1) {
                swap(nums[i], nums[w]);
                w++;
            } else {
                continue;
            }
        }
    }
};
