/*#include <iostream>
#include <string>
#include <vector>
using namespace std;
int main()
{
  string s1("hello world");
  string::iterator it = s1.begin();
  while (it != s1.end())    //必须要使用！=
  {
    cout << *it << " ";
    it++;
  }
  cout << endl;
  vector<int> v = {1,2,3,4};
  vector<int>::iterator vit = v.begin();
  while(vit != v.end())
  {
    cout << *it << " ";
    it++;
  }
  cout << endl;
  return 0;

}*/
#include <iostream>
#include <vector>
#include <string>
using namespace std;


int main()
{
    string s1("hello world");
    string::iterator it = s1.begin();
    while (it != s1.end())
    {
        cout << *it << " ";
        it++;
    }
    cout << endl;
    string s2("123456");
    string::reverse_iterator rit = s2.rbegin();
    while (rit != s2.rend())
    {
      cout <<*rit << " ";
      rit++;
    }
    return 0;
}

