class Solution {
    public int lengthOfLongestSubstring(String s) {
        HashMap<Character, Integer> book = new HashMap<Character, Integer>();

        Integer result = 0, begin = 0;
        for (int i = 0; i < s.length(); i++) {
            if (book.containsKey(s.charAt(i)) && book.get(s.charAt(i)) >= begin) {
                begin = i + 1;
            }
            book.put(s.charAt(i), i);
            result = result > i - begin + 1 ? result : i - begin + 1;
        }
        return result;
    }
}
