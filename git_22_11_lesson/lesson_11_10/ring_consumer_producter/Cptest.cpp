#include "ring_queue.h"
using namespace ns_ring_queue;

void* con_run(void* args){
    RingQueue<int>* rq = (RingQueue<int>*)args;
    while (true){
        int data = 0;
        rq->Pop(&data);
        std::cout << "消费了一个数据 " << data << std::endl;
        sleep(1);
    }
}

void* pro_run(void* args){
    RingQueue<int>* rq = (RingQueue<int>*)args;
    while (true){
        int data = rand() % 20 + 1;
        rq->Push(data);
        std::cout << "生产了一个数据" << data << std::endl;
        sleep(1);
    }
}


int main(){
    srand((long long)time(nullptr));
    pthread_t con, pro;
    RingQueue<int>* rq = new RingQueue<int>(10);
    pthread_create(&con, nullptr, con_run, (void*)rq);
    pthread_create(&pro, nullptr, pro_run, (void*)rq);
    pthread_join(con, nullptr);
    pthread_join(pro, nullptr);
    return 0;
}
