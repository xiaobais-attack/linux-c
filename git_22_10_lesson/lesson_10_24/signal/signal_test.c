#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void handler(int signo){
  switch(signo){
    case 2:
    printf("get a signal !!! signal NO = %d, process pid = %d\n", signo, getpid());
    break;

    case 3:
    printf("get a signal !!! signal NO = %d, process pid = %d\n", signo, getpid());
    break;

    case 9:
    printf("get a signal !!! signal NO = %d, process pid = %d\n", signo, getpid());
    break;
    default:
    break;
  }
}
int main()
{
  for (int i = 0; i < 31; i++){
    signal(i, handler);
  }

  while (1){
    printf("hello world, mypid = %d\n", getpid());
    sleep(1);
  }
  return 0;
}
