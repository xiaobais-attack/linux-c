#include <iostream>
#include <unistd.h>
#include <signal.h>
using namespace std;

void handler(int sigNo){
  cout << "get a signal , No = " << sigNo << endl;;
}
int main(){
  for (int i = 1; i < 32; i++){
    signal(i, handler);
  }
  sleep(10);
  return 0;
}
