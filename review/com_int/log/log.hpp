#pragma once 
#include <time.h>
#include <iostream>
#include "level.hpp"

static const std::string log_filename = "./log/tcp_server.log";

// 日志格式 [日志等级][时间][文件][行][PID][消息体]

namespace clx {
    static std::string getTime() {
        char logLeft[1024];
        time_t t = time(nullptr);
        char buffer[64];
        strftime(buffer, sizeof(buffer), "%Y-%m-%d %H-%M-%S", localtime(&t)); //年-月-日 时-分-秒
        return buffer;
    }

    static void logMessage(int level, const char* format, ...) {
    }
}