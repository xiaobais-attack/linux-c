#include <stdio.h>
#include <unistd.h>

int main()
{
  printf("I'm running ... \n");
  pid_t id = fork();
  if (id == 0)
  {
    while (1)
    {
      printf("do something\n");
      sleep(1);
    }
  }
  else if(id > 0)
  {
    while(1)
    {
      printf("call child do something!\n");
      sleep(2);
    }
  }
  else
  {
    ;
  }
  return 0;

  
 
}
