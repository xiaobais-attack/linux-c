#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdio.h>

#define LEN 128
#define CMD_NUM 64
int main()
{
  char command[LEN] = {0};
  for (;1;)
  {
    command[0] = 0;
    char* argv[CMD_NUM] = {NULL};
    printf("[clx@workhosmydir]#");
    fflush(stdout);
    fgets(command, LEN, stdin);
    command[strlen(command) - 1] = 0;
    const char* sep = " ";
    argv[0] = strtok(command, sep);
    size_t i = 1;
    while (argv[i] = strtok(NULL, sep))
    {
      i++;
    }
    //for (int i = 0; argv[i] != 0; i++)
    //{
    //  printf("argv[%d] = %s\n", i, argv[i]);
    //}
    
    if(fork() == 0)
    {
      execvp(argv[0], argv);
      exit(1);
    }
    waitpid(-1, NULL, 0);
  }
}




























//int main()
//{
//  while (1)
//  {
//     printf("[clx@myhosname mydir]# ");
//     fflush(stdout);
//     sleep(1);
//     char file[100];
//     char* argv[100] = {0};
//     file[0] = 0;
//     while (scanf("%s", file) != EOF)
//     {
//       for (size_t i = 0; i < 100; i++)
//       {
//         argv[i] = NULL;
//       }
//       char* paramater; 
//       size_t count = 0;
//       while (scanf("%s", paramater))
//       {
//         argv[count] = paramater;
//         if (argv[count] == NULL)
//         {
//           break;
//         }
//         count++;
//       }
//       pid_t id = fork();
//       if (id == 0)
//       {
//         execvp(file, argv);
//         exit(1);
//       }
//       int status = 0;
//       pid_t ret = waitpid(-1, &status, 0);
//       if (ret > 0 && WIFEXITED(status))
//       {
//         printf("father wait success\n");
//         if (WEXITSTATUS(status) == 1)
//         {
//           printf("child task failed\n");
//         }
//         else 
//         {
//           printf("child task success\n");
//         }
//       }
//       else 
//       {
//         printf("child faild\n");
//       }
//     }
//  }
//  return 0;
//}
