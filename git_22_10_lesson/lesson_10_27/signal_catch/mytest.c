#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>


void handler(int signum)
{
  int count = 0;
  while (++count <= 10){
    printf("get a signal %d, process's pid = %d\n", signum, getpid());
    sleep(1);
  }
}

int main()
{
  struct sigaction act;
  memset(&act, sizeof(act), 0);
  sigset_t mask;
  sigemptyset(&mask);
  sigaddset(&mask, 3);
  //act.sa_handler = SIG_IGN;
  
  act.sa_handler = handler;
  act.sa_mask = mask;
  sigaction(2, &act, NULL);
  while (1){
    sleep(5);
    printf("I'm running\n");
  }

  return 0;
}
