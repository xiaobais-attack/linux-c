class Solution {
public:
    void QuickSort(vector<int>& nums, int left, int right) {
        if (left >= right) return;
        int base = nums[left], begin = left, end = right;
        while (begin < end) {
            while (begin < end && nums[end] >= base) end--;
            while (begin < end && nums[begin] <= base) begin++;
            swap(nums[begin], nums[end]);
        }
        swap(nums[left], nums[begin]);
        QuickSort(nums, left, begin - 1);
        QuickSort(nums, begin + 1, right);
    }
    vector<int> sortArray(vector<int>& nums) {
        QuickSort(nums, 0, nums.size() - 1);
        return nums;
    }
};
