#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

void* thread_code(void* args)
{
  const char* id = (const char*)args;
  while (1){
    printf("我是%s, mypid = %d\n", id, getpid());
    sleep(1);
  }
}
int main()
{
  pthread_t tid;
  pthread_create(&tid, NULL, thread_code, (void*)"child thread");
  while (1){
    printf("我是main 线程， mypid = %d\n", getpid());
    sleep(1);
  }

}
