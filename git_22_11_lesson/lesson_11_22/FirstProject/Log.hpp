#pragma once
#include <iostream>
#include <string>

#define INFO     0
#define WARNIN   1
#define ERROR    2
#define FATAL    3
#define LOG(level, message) Log(#level, message, __FILE__, __LINE__)

void Log(std::string level, std::string message, std::string file_name, int line){
   std::cout << "[" <<level << "]" << "[" << time(nullptr) << "]" << "[" << message << "]" << "[" << file_name  << "]"<< "[" << line << "]" << std::endl; 
}
