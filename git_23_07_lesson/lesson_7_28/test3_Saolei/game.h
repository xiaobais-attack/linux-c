#pragma once 
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#define ROW 9
#define COL 9
#define ROWS ROW+2
#define COLS COL+2
#define BOMB_COUNT 10

void Menu();
void InitBoard(char board[ROWS][COLS], size_t row, size_t col, char ch);
void DisplayBoard(char board[ROWS][COLS]);
void SetMine(char board[ROWS][COLS]);
void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS]);
size_t get_mine_count(char mine[ROWS][COLS], size_t x, size_t  y);

