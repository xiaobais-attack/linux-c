#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#define LEN 1024
#define NUM 32


int main()
{
  while (1)
  {
    char cmd[LEN];
    char* myarg[NUM];
    printf("[clx@my-centos_mc dir]$ ");
    //获取输入
    fgets(cmd, LEN, stdin); 
    cmd[strlen(cmd) - 1] = '\0';
    //解释输入
    myarg[0] = strtok(cmd, " ");
    int i = 1;
    while (myarg[i] = strtok(NULL, " "))
    {
      i++;
    }
    pid_t id = fork();
    if (id == 0)
    {
      //child 
      execvp(myarg[0], myarg);
    }
    int status = 0;
    pid_t ret = waitpid(id, &status, 0);
    if (ret > 0)
    {
      printf("wait success!\n");
      printf("signal: %d\n", status & 0x7F);
      printf("exit code: %d\n", WEXITSTATUS(status));
    }
    
    
    
  }
  return 0;
}
