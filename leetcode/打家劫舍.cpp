class Solution {
public:
    int rob(vector<int>& nums) {
        vector<int> dp(nums.size() + 2, 0);

        int result = 0;
        for (int i = 2; i < nums.size() + 2; i++) {
            dp[i] = max(nums[i - 2] + dp[i - 2], dp[i - 1]);
        }
        return dp[dp.size() - 1];
    }
};
