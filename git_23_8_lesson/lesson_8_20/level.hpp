/*
    1、定义枚举类，枚举出日志等级
    2、提供转换接口：将美剧转换为对应字符串
*/
#ifndef __CLX_LEVEL_H__
#define __CLX_LEVEL_H__

namespace clxlog
{
    class LogLevel
    {
    public:
        enum value
        {
            UNKNOW = 0,
            DEBUG,
            INFO,
            WARN,
            ERROR,
            FATAL,
            OFF
        };

        static const char *toString(LogLevel::value level){
            switch (level){
                #define TOSTRING(name) #name
                case LogLevel::value::UNKNOW: return TOSTRING(UNKNOW);
                case LogLevel::value::DEBUG : return TOSTRING(DEBUG);
                case LogLevel::value::INFO  : return TOSTRING(INFO);
                case LogLevel::value::WARN  : return TOSTRING(WARN);
                case LogLevel::value::ERROR : return TOSTRING(ERROR);
                case LogLevel::value::FATAL : return TOSTRING(FATAL);
                case LogLevel::value::OFF   : return TOSTRING(OFF);
                #undef TOSTRING
            }
            return "UNKNOW";
        }
    };
}
#endif
