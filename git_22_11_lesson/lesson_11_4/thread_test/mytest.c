#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define THD_NUM 5

//void* thread_run(void* thread_name)
//{
//  while (1){
//    printf("I'm %s, my tid = %lu\n", thread_name, pthread_self());
//    sleep(1);
//  }
//}

//void test1()
//{
//  pthread_t tid;
//  pthread_create(&tid, NULL, thread_run, (void*)"child thread");
//  while (1){
//    printf("I'm main thread, my tid = %lu\n", pthread_self());
//    sleep(1);
//  }
//}

//void* thread_run(void* thread_index)
//{
//  int index = *(int*)thread_index;
//  while (1){
//    printf("child thread[%d], tid = %lu\n", index, pthread_self());
//    if (*(int*)thread_index == 2){
//      printf("%d", 1 / 0);
//    }
//    sleep(1);
//  }
//}
//
//void test2()
//{
//  pthread_t tid_arr[THD_NUM];
//  for (int i = 0; i < THD_NUM; i++){
//    pthread_create(&tid_arr[i], NULL, thread_run, (void*)&i);
//    sleep(2);
//  }
//  printf("####################begin########################\n");
//  for (int i = 0; i < THD_NUM; i++){
//    printf("child[%d], tid = %lu\n", i, tid_arr[i]);
//  }
//  printf("####################End##########################\n");
//  while (1){
//    printf("I'm main thread, my tid = %lu\n", pthread_self());
//    sleep(1);
//  }
//}

//typedef struct thread_info{
//  int _index;
//  int _exit_code;
//  pthread_t _tid;
//}thread_info;
//
//void* thread_run(void* thread_index)
//{
//  int index = *(int*)thread_index;
//  thread_info* info = (thread_info*)malloc(sizeof(thread_info));
//  info->_index = index;
//  info->_exit_code = 0;
//  info->_tid = pthread_self();
//  printf("child thread[%d], tid = %lu\n", index, pthread_self());
//  return info;
//}
//
//void test3()
//{
//  //线程创建
//  pthread_t tid_arr[THD_NUM];
//  for (int i = 0; i < THD_NUM; i++){
//    pthread_create(&tid_arr[i], NULL, thread_run, (void*)&i);
//    sleep(2);
//  }
//
//  //线程等待
//  thread_info* thread_info_arr[THD_NUM];
//  for (int i = 0; i < THD_NUM; i++){
//    pthread_join(tid_arr[i], (void**)(thread_info_arr + i));
//  }
//  //打印线程退出信息
//  for (int i = 0; i < THD_NUM; i++){
//    printf("%lu", thread_info_arr[i]->_tid);
//  }
//
//  while (1){
//    sleep(1);
//  }
//}
//





void* thread_run(void* tid){
  sleep(5);
  pthread_t ptid = *(pthread_t*)tid; 
  void* status = NULL;
  pthread_cancel(ptid);
  pthread_join(ptid, (void**)&status);
  printf("status = %d", (int)status);
  while (1){
    printf("I'm child\n");
    sleep(1);
  }
  return(void*)123;
}


void test4(){
  printf("I'm running \n");
  printf("I'm running \n");
  printf("I'm running \n");
  printf("I'm running \n");
  pthread_t tid;
  pthread_t self_id = pthread_self();
  pthread_create(&tid, NULL, thread_run, (void*)&self_id);
  return;
  //sleep(5);
  //pthread_cancel(pthread_self());
  //void* status = NULL;
  //pthread_join(tid, (void**)&status);
  //printf("%ld\n", status);
  //sleep(10);
}
int main()
{
  test4();
  return 100;
}

