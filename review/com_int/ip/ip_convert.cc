#include <iostream>
#include <vector>
using namespace std;

struct segmentation_ip {
  uint32_t p1 : 8;
  uint32_t p2 : 8;
  uint32_t p3 : 8;
  uint32_t p4 : 8;
};

union IP {
  uint32_t ip; // 整数IP
  struct segmentation_ip p;
};

int main() {
  uint32_t max_ip = -1;
  union IP my_ip = {max_ip};
  printf("%d.%d.%d.%d\n", my_ip.p.p1, my_ip.p.p2, my_ip.p.p3, my_ip.p.p4);
  return 0;
}
