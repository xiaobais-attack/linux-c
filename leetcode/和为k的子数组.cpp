class Solution {
public:
    int subarraySum(vector<int>& nums, int k) {
        unordered_map<int, int> book;
        book[0] = 1;
        int result = 0, sum = 0;
        for (int i = 0; i < nums.size(); i++) {
            sum += nums[i];
            if (book.find(sum - k) != book.end()) {
                result += book[sum - k];
            }
            book[sum]++;
        }
        return result;
    }
};
