class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        vector<int> newArr(nums.size(), 0);
        int n = nums.size();
        for (int i = 0; i < nums.size(); i++) {
            newArr[i] = nums[(i + n - k) % n];
        }
        for (int i = 0; i < nums.size(); i++) {
            nums[i] = newArr[i];
        }
        

    }
};
