#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{
  printf("I'm running .. \n");
  pid_t id = fork();

  if (id == 0)
  {
    int count = 5;
    while(count)
    {
      printf("I'm child .. pid = %d , ppid = %d\n", getpid(), getppid());
      sleep(1);
      count--;
    }
    exit(1);
  }
  else if(id > 0)
  {
    int count = 3;
    while(count)
    {
      printf("I'm father .. coumt = %d, pid = %d, ppid = %d\n", count, getpid(), getppid());
      count--;
      sleep(1);
    }
    exit(2);
  }
  else{
    printf("I'm fail..\n");
  }
  return 0;
}

