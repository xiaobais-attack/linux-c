class Solution {
public:
    bool isMatch(string s, string p) {
        int m = s.size() + 1, n = p.size() + 1;
        vector<vector<bool>> dp(m, vector<bool>(n, false));
        dp[0][0] = true;

        auto comp = [&](int i, int j){
            if (i == 0) return false;
            if (p[j - 1] == '.') return true;
            return s[i - 1] == p[j - 1];
        };

        for (int i = 0; i < m; i++) {
            for (int j = 1; j < n; j++) {
                if (p[j - 1] == '*') {
                    dp[i][j] = dp[i][j - 2];
                    if (comp(i, j - 1)) {
                        dp[i][j] = dp[i][j] | dp[i - 1][j];
                    }
                }
                else {
                    if (comp(i, j)) {
                        dp[i][j] = dp[i - 1][j - 1];
                    }
                }
            }
        }
        return dp[m - 1][n - 1];


    }
};
