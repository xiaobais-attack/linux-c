#include <iostream>
#include <vector>
#include <list>
using namespace std;

namespace clx 
{
  template<class T, class Container = std::vector<T>>
  class stack
  {
    public:
      
      bool empty() const
      {
        _con.empty();
      }
      size_t size() const
      {
        _con.size();
      }
      const T& top() const
      {
        return _con.back(); 
      }

      void push(const T& x)
      {
        _con.push_back(x);
      }
      void pop()
      {
        _con.pop_back();
      }
      void swap(stack<T>& x)
      { 
        swap(x);
      }
    private:
      Container _con;
  };
  void stack_test();
}

void clx::stack_test()
{
  stack<int> st;
  st.push(1);
  st.push(2);
  st.push(3);
  st.push(4);
  while (!st.empty())
  {
    cout << st.top() << " ";
    st.pop();
  }
  cout << endl;


}
