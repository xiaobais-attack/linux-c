#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{

  printf("I'm running ..\n");
  pid_t id = fork();
  if (id == 0)
  {
    while(1)
    {
      int count = 5;
      while (count)
      {
        printf("I'm child pid = %d , ppid = %d  cout = %d ..\n", getpid(), getppid(), count);
        count--;
        sleep(1);
      }
      printf("I'm quit ...\n");
      exit(1);
    }
  }
  else if(id > 0)
  {
    while(1)
    {
      printf("I'm father process .. pid = %d, ppid = %d\n", getpid(), getppid());
      sleep(1);
    }
  }


  return 0;
}
