#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>

int count = 0;
//void handler(int signum)
//{
//  printf("hello : %d\n", count);
//  exit(1);
//}

int main()
{
  alarm(1);
  while (1)
  {
    printf("hello : %d\n", count);
    count++;
  }
  return 0;
}

//void handler(int signum)
//{
//  printf("signum = %d\n", signum);
//}
//
//
//int main()
//{
//
//  for (int i = 1; i <= 31; i++){
//    signal(i, handler);
//  }
//  alarm(3);
//  while (1){
//    sleep(1);
//    printf("I'm a process !! pid = %d\n", getpid());
//  }
//  return 0;
//}
//
//










//void test1()
//{
//  if (fork() == 0){
//    printf("I'm child\n");
//  }
//  int status = 0;
//  waitpid(-1, &status, 0);
//  printf("child exit code = %d, child get signal = %d, core dump flag = %d\n", (status >> 8) & 0xff, status & 0x7f, (status >> 7) & 1);
//  printf("father end\n");
//}
//
//
//void test2(){
//  while (1)
//  {
//    sleep(1);
//    printf("I'm a process\n");
//  }
//}
//
//
//void Usage(const char* proc){
//  printf("Usage:\n\t %s signo who\n", proc);
//}
//
//int main(int argc, char *argv[])
//{
//  if (argc != 3){
//    Usage(argv[0]);
//    return 1;
//  }
//  int who = atoi(argv[2]);
//  int signal = atoi(argv[1]);
//  kill(who, signal);
//  printf("signal = %d, who = %d\n", signal, who);
//}
