#ifndef __CLX_SINK_HPP__
#define __CLX_SINK_HPP__ 

#include "util.hpp"
#include <memory>
#include <fstream>
#include <cassert>

namespace clxlog {
  class Sink {
    public:
      using ptr = std::shared_ptr<Sink>;
      Sink() {};
      virtual ~Sink() {};
      virtual void log(const char* data, int len) = 0;
  };

  class StdoutSink : public Sink{
    public:
      void log(const char* data, int len) override{
        std::cout.write(data, len);
      }
  };

  class FileSink : public Sink{
    public:
      FileSink(const std::string& path);
      void log(const char* data, int len) override{
        // 创建文件所在的路径
          util::File::createDirectory(util::File::path(_pathname));
        // 打开日志文件
        _ofs.open(_pathname, std::ios::binary | std::ios::app);
        assert(_ofs.is_open()); 
        // 写入数据
        _ofs.write(data, len);
        assert(_ofs.good());
      }
    private:
      std::string _pathname;
      std::ofstream _ofs;
  };

  class SinkFactory {
    using ptr = std::shared_ptr<SinkFactory>;
    template<class SinkType, class ...Args>
      static Sink::ptr create(Args&&... args) {
        return std::make_shared<SinkType>(std::forward<Args>(args)...);
      }
  };
}
#endif
