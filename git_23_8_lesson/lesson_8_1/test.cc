#include <iostream>
#include <algorithm>
using namespace std;

const int N = 1010;

int n;
int a[N], f[N];

int main(){
    scanf("%d", &n);
    for (int i = 1; i <= n; i++) scanf("%d", &a[i]);
    
    int res = 0;
    for (int i = 1; i <= n; i++) {
        f[i] = a[i];
        int t = a[i];
        for (int j = 1; j < i; j++) {
            if (a[i] > a[j]) {
                f[i] = max(f[i], f[j] + t);
            }
        }
        res = max(res, f[i]);
    }
    printf("%d\n", res);
    
    return 0;
}
