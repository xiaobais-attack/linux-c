#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>

int main()
{
  pid_t id = fork();
  if (id == 0)
  {
    //child 
    printf("I am child process! ..\n");
    sleep(3);
    //execl("/usr/bin/ls", "ls", "-a", "-l", "-i", NULL);
    //execlp("ls", "ls", "-a", "-l", "-i", NULL);
    //char* myargv[] = { "ls", "-a", "-l", "-i", NULL};
    //execv("/usr/bin/ls", myargv);
    //execvp("ls", myargv);
    char* myenv[] = {"MYENV=youcanseemeHAHA", NULL};
    execle("./cmd", "cmd", NULL, myenv);
    //execl("./test.sh", "test.sh");
    exit(10);
  }

  int status = 0;
  pid_t ret = waitpid(id, &status, 0);
  if (ret > 0)
  {
    printf("signal: %d\n", status & 0x7a);
    printf("exit code: %d\n", WEXITSTATUS(status));
  }
  return 0;
}
