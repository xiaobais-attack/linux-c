#include <stdio.h>
#include <signal.h>
#include <unistd.h>


int flag = 0;
void handler(int signum)
{
    printf("get a signal = %d, my pid = %d\n", signum, getpid());
    printf("chang flag 0 to 1\n");
    flag = 1;
}

int main()
{
  signal(3, handler);
  while (!flag);
  return 0;
}
