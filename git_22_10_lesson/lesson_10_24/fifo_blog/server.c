#include "comm.h"

int main()
{
  //若命名管道不存在则创建，成功返回0失败返回-1
  if (mkfifo(FIFO_PATH, FIFO_MODE) != 0){
    perror("mkfifo failed\n");
    exit(1);
  }

  int fd = open(FIFO_PATH, O_RDONLY);
  if (fd < 0){
    perror("open failed\n");
    exit(2);
  }
  //业务逻辑
  
  while (1){
    char buffer[64] = {0};
    int s = read(fd, buffer, sizeof(buffer) - 1); //ls\n
    buffer[s - 1] = 0;
    if (strcmp(buffer, "show")){
      if (fork() == 0){
        execl("/usr/bin/ls", "ls", "-a", "-l", "-i", NULL);
        exit(3);
      }
    }
    else if (strcmp(buffer, "run")){
      if (fork() == 0){
        execl("/usr/bin/sl", "sl", NULL);
      }
    }
    else {
      printf("client say : %s\n", buffer);
    }
  }
  close(fd);

  return 0;
}
