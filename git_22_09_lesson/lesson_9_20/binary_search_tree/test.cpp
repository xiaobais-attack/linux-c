#include "BST.h"

void BSTreeTest1()
{
  BSTree<int> t;
  int arr[] = {5,4,2,6,1,8,3};
  for (auto e : arr)
  {
    t.Insert(e);
  }
  t.InOrder();
}

void BSTreeTest2()
{
  BSTree<int> t;
  int arr[] = {5,4,2,6,8,3,2,1};
  for (auto e : arr)
  {
    t.Insert(e);
  }
  t.InOrder();
  t.Erase(5);
  t.InOrder();
}

int main()
{
  //BSTreeTest1();
  BSTreeTest2();
  return 0;

}
