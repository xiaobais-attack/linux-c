/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int deep(TreeNode* root) {
        if (root == nullptr) return 0;
        int left = deep(root->left);
        int right = deep(root->right);
        if (left == -1 || right == -1 || abs(left - right) > 1) {
            return -1;
        }
        else {
            return max(left, right) + 1;
        }
    }
    bool isBalanced(TreeNode* root) {
        return deep(root) >= 0;

    }
};
