#include <stdio.h>


int main()
{
  int i = 1, sum = 0;
  for (i = 1; i < 101; i++)
  {
    if (i % 10 == 9)
    {
      sum++;
    }
    if (i / 10 == 9)
    {
      sum++;
    }
  }
  printf("%d\n", sum);
  return 0;
}
