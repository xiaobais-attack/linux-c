#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>


int main()
{
  umask(0);
  int fd = open("log.txt", O_WRONLY|O_CREAT, 0666);
  if (fd < 0)
  {
    perror("open erroe");
    return 1;
  }
  close(1);
  dup2(fd, 1);
  printf("hello printf\n");
  fprintf(stdout, "hello fprintf\n");
  fputs("hello puts", stdout);

  //fclose(stdout);
  //close(1);
  
}
