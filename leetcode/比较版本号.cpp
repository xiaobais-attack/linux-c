class Solution {
public:

    int GetSubVersion(string& version, int& cur) {
        string result;
        while (cur < version.size() && version[cur] == '0') cur++;
        while (cur < version.size() && version[cur] != '.') {
            result.push_back(version[cur]);
            cur++;
        }
        return result.empty() ? 0 : stoi(result);

    }
    int compareVersion(string version1, string version2) {
        int cur1 = 0, cur2 = 0;
        while (cur1 < version1.size() || cur2 < version2.size()) {
            int sub_ver1 = 0, sub_ver2 = 0;
            sub_ver1 = GetSubVersion(version1, cur1);
            sub_ver2 = GetSubVersion(version2, cur2);
            if (cur1 < version1.size()) cur1++;
            if (cur2 < version2.size()) cur2++;
            
            if (sub_ver1 > sub_ver2) return 1;
            if (sub_ver1 < sub_ver2) return -1;
        }
        return 0;
    }
};
