#include <iostream>
#include <memory>
#include "EpollServer.hpp"
#include "Log.hpp"

std::string echoServer(std::string r) {
    std::string resp = r;
    resp += "[echo]\r\n";
    return resp;
}

int main() {
    std::unique_ptr<EpollServer> svr(new EpollServer(echoServer));
    svr->InitServer();
    svr->Start();
}
