#include "game.h"

void Menu()
{
  printf("\n");
  printf("welcome to the three chess game\n");
  printf("-------------------------------\n");
  printf("----1.Start         2.out------\n");
  printf("-------------------------------\n");
  printf("\n");
}

void InitBoard(char board[ROWS][COLS], size_t row, size_t col, char ch)
{
  for (size_t i = 0; i < row; i++)
  {
    for (size_t j = 0; j < col; j++)
    {
      board[i][j] = ch;
    }
  }
}
void DisplayBoard(char board[ROWS][COLS])
{
  printf("  ");
  for(size_t i = 1; i <= COL; i++)
  {
    printf(" %lu ", i);
  }
  printf("\n");
  for (size_t i = 1; i <= ROW; i++)
  {
    printf("%lu ", i);
    for (size_t j = 1; j <= COL; j++)
    {
      printf(" %c ", board[i][j] );
    }
    printf("\n");
  }
  printf("\n\n");
}
void SetMine(char board[ROWS][COLS])
{
  size_t count = BOMB_COUNT; 
  while (count)
  {
    size_t row = rand() % ROW + 1;
    size_t col = rand() % COL + 1;
    if (board[row][col] == '0')
    {
      board[row][col] = '1';
      count--;
    }

  }
}
void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS])
{
  size_t x, y;
  size_t count = ROW * COL - BOMB_COUNT;
  while (1)
  {
    printf("请输入坐标(x,y)并用空格分开:>");
    scanf("%lu%lu", &x, &y);
    if (x >= 1 && x <= ROW && y >= 1 && y <= COL)
    {
      if (mine[x][y] == '1')
      {
        DisplayBoard(mine);
        printf("踩到雷了，你输了\n");
        break;
      }
      else 
      {
        show[x][y] = get_mine_count(mine, x, y) + '0';
        DisplayBoard(show);
        count--;
      }
    }
    else 
    {
      printf("输入错误坐标，请重新输入\n");
    }
    if (count == 0)
    {
      printf("恭喜你，你赢了\n");
      DisplayBoard(mine);
      break;
    }
  }


}
size_t get_mine_count(char mine[ROWS][COLS], size_t x, size_t  y)
{
  size_t count = 0;
  for (size_t i = x -1; i <= x + 1; i++)
  {
    for (size_t j = y - 1; j <= y + 1; j++)
    {
      if (mine[i][j] == '1')
      {
        count++;
      }
    }
  }
  return count;
}
