#include "comm.h"
int main()
{
  int fd = open(MY_FIFO, O_WRONLY);
  if (fd < 0){
    perror("open error\n");
    exit(1);
  }

  while (1){
    printf("请输入#");
    fflush(stdout);
    char buffer[64] = {0};
    //先把数据从标准输出拿到我们的client进程内部
    size_t s = read(0, buffer, sizeof(buffer) - 1);
    if (s > 0){
      buffer[s - 1] = 0;
      //printf("%s", buffer);
    }
    //拿到了数据
    write(fd, buffer, strlen(buffer));
  }
  return 0;
}
