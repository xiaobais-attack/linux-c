#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


void handler(int signum){
  //int status = 0;
  //waitpid(-1, &status, 0);
  //printf("get a %d singal\n", signum);
  //printf("exit code = %d, exit signal = %d\n", (status >> 8) & 0xff, status & 0x7f);
  pid_t id;
  while ((id = waitpid(-1, NULL, WNOHANG)) > 0)
    printf("wait pid = %d success, get pid = %d\n", id, getpid());
}

int main()
{
  if (fork() == 0){
    int count = 3;
    while (count--){
      printf("I'm child, mypid = %d\n", getpid());
      sleep(1);
    }
    printf("child pid = %d", getpid());
    exit(0);
  }
  signal(SIGCHLD, handler);
  //显式设置忽略17号信号，当进程退出后，自动释放僵尸进程
  //只在linux下有效
  //signal(SIGCHLD, SIG_IGN);
  while (1);
  return 0;
}
