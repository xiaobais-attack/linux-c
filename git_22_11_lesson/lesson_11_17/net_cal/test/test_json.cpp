#include <iostream>
#include <string>
#include <jsoncpp/json/json.h>

typedef struct request_t
{
    int num1;
    int num2;
    int op;
} request_t;

int main()
{
    request_t req = {10, 20, '*'};
    Json::Value root;
    root["datax"] = req.num1;
    root["datay"] = req.num2;
    root["operator"] = req.op;

    // FastWrite StyledWrite
    Json::StyledWriter writer;
    std::string json_string = writer.write(root);

    std::cout << json_string << std::endl;
    return 0;
}