#include "poll_server.hpp"
#include <string>


void Usage(char* proc) {
  std::cout << "Usage: " << proc << " port" << std::endl;
}

int main(int argc, char* argv[]) {
  if (argc != 2) {
    Usage(argv[0]);
    exit(1);
  }

  int port = atoi(argv[1]);
  PollServer* ps = PollServer::GetInstance(port);
  ps->InitPollServer();
  ps->Run();
  return 0;

}

