#include "comm.h"

void Usage(std::string proc)
{
  std::cout << "Usage: \n\t" << proc << "server_ip server_prot" << std::endl;
}

int main(int argc, char *argv[])
{
  if (argc != 3)
  {
    Usage(argv[0]);
    return 0;
  }
  int sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock < 0)
  {
    std::cerr << "socket failed" << errno << std::endl;
    return 1;
  }

  struct sockaddr_in server;
  server.sin_family = AF_INET;
  server.sin_port = htons(atoi(argv[2]));
  server.sin_addr.s_addr = INADDR_ANY;

  while (true)
  {
    std::string message;
    std::cout << "请输入:";
    std::cin >> message;

    sendto(sock, message.c_str(), message.size(), 0, (struct sockaddr *)&server, sizeof(server));

    struct sockaddr_in tmp;
    socklen_t len = sizeof(tmp);
    char buffer[1024];
    recvfrom(sock, buffer, sizeof(buffer), 0, (struct sockaddr *)&tmp, &len);
    std::cout << "server echo:" << buffer << std::endl;
  }
  return 0;
}
