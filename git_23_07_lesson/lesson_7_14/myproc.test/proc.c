#include "proc.h"

void ProcBar()
{
  int i = 0;
  char proc[102]; 
  const char* lable = "|/-\\";
  memset(proc, '\0',sizeof(proc));

  while (i < 101)
  {
    printf("[%-100s][%d%%][%c]\r", proc, i, lable[i%4]);
    fflush(stdout);
    proc[i] = '#';
    usleep(100000);
    i++;
  }
  printf("\n");

} 


/*void ProcBar()
{
  int i = 0;
  char proc[102];
  memset(proc, '\0', sizeof(proc));
  const char* lable = "|/-\\";
  while(i <= 100)
  {
    printf("[%-100s][%d%%][%c]\r", proc, i, lable[i%4]);
    fflush(stdout);
    proc[i] = '#';
    usleep(100000);
    i++;
  }
  printf("\n");
  }*/ 
