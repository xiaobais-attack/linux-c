#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>


int main()
{
  umask(0);
  int fd1 = open("log.txt", O_WRONLY | O_CREAT, 0666);
  if (fd1 < 0)
  {
    printf("file open error\n");
    return 1;
  }
  printf("%d\n", fd1);

  char c;
  while (1)
  {
    ssize_t s = read(fd1, &c, 1);
    if (s <= 0)
    {
      break;
    }
    write(1, &c, 1);
  }
  //int count = 5;
  //const char* msg = "hello world\n";
  //while (count)
  //{
  //  write(fd1, msg, strlen(msg));
  //  count--;
  //}
  //close(fd1);

  return 0;
}
