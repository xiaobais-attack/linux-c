#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main()
{
  pid_t id = fork();
  if (id == 0)
  {
    //child 
    printf("I am child\n");
    execl("/usr/bin/ls", "-a", "-i", "-l", NULL);
    exit(10);
  }

  int status = 0;
  pid_t ret = waitpid(id, &status, 0);
  if (ret > 0)
  {
    printf("signal: %d\n", status & 0x7F);
    printf("exit code: %d\n", WEXITSTATUS(status));
  }
  return 0;
  //printf("I am a process!\n");
  //sleep(5);
  //execl("/usr/bin/ls", "ls", "-a", "-i", "-l", NULL);
  //
  //printf("you can see me!\n");
  //pid_t id = fork();
  //if (id == 0)
  //{
  //  //child
  //  int count = 0;
  //  while (count < 10)
  //  {
  //    printf("I am child.. pid: %d, ppid: %d\n", getpid(), getppid());
  //    sleep(3);
  //    count++;
  //  }
  //  exit(1);
  //}

  //while (1)
  //{
  //  int status = 0;
  //  pid_t ret = waitpid(id, &status, WNOHANG);
  //  if (ret > 0)
  //  {
  //    printf("wait success!\n");
  //    printf("wait exit code: %d\n", WEXITSTATUS(status));
  //    break;
  //  }
  //  else if (ret == 0)
  //  {
  //    //child not quit, waitpid success
  //    printf("father do other things!\n");
  //    sleep(1);
  //  }
  //  else 
  //  {
  //    printf("waitpid error!\n");
  //    break;
  //  }
  //  printf("ret: %d\n", ret);
  //}
}
