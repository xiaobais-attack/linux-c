#include "BlockQueue.h"
#include <unistd.h>

using namespace my_blockq; 


void* con_run(void* args){
  BlockQueue<int>* bq = (BlockQueue<int>*)args;
  while (true){
    int data = 0;
    bq->Pop(&data);
    std::cout << "消费者消费了一个数据 " << data << std::endl;
    sleep(1);
  }

}

void* pro_run(void* args){
  BlockQueue<int>* bq = (BlockQueue<int>*)args;
  while (true){
    int data = rand() % 30 + 1;
    bq->Push(data);
    std::cout<< "生产者生产了一个数据 " << data << std::endl;
    sleep(1);
  }

}

int main(){
  srand((long long)time(nullptr));
  BlockQueue<int> *bq = new BlockQueue<int>(5);
  pthread_t con, pro;
  pthread_create(&con, nullptr, con_run, (void*)bq);
  pthread_create(&pro, nullptr, pro_run, (void*)bq);
  pthread_join(con, nullptr);
  pthread_join(pro, nullptr);


}
