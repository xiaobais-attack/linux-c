#ifndef __CLX_UTIL_HPP__
#define __CLX_UTIL_HPP__

#include <iostream>
#include <string>
#include <ctime>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

namespace clxlog {
  namespace util {
    class Date {
      public:
      static time_t now() { return (time(nullptr)); }
    };

    class File {
      public:
      static bool exists(const std::string& pathname) {
        struct stat st;
        if (stat(pathname.c_str(), &st) < 0)  return false;
        return true;
      }

      static std::string path(const std::string& pathname) {
        std::string seperator = "/\\";
        size_t pos = pathname.find_last_of(seperator);
        if (pos == std::string::npos) return "./";
        std::string path = pathname.substr(0, pos + 1);
        return path;
      }

      static void createDirectory(const std::string pathname) {
        size_t pos = 0;
        std::string seperator = "/\\";
        while (1) {
          pos = pathname.find_first_of(seperator, pos);
          if (pos == std::string::npos) {
            mkdir(pathname.c_str(), 0777);
            break;
          }

          std::string sub_path = pathname.substr(0, pos + 1);
          if (!exists(sub_path)) {
            mkdir(sub_path.c_str(), 0777);
          }
          pos = pos + 1;
        }
      }

    };
  }
}

#endif
