class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        if (nums.empty()) return {-1, -1};
        int left = 0, right = nums.size() - 1;
        while (left <= right && !((nums[left] == target) && (nums[right] == target))) {
            int mid = (left + right) / 2;
            if (nums[mid] > target) {
                right = mid - 1;
            }
            else if (nums[mid] < target) {
                left = mid + 1;
            }
            else {
                if (nums[left] == target) right--;
                else if (nums[right] == target) left++;
                else {
                    right--;
                    left++;
                }
            }
            cout << "left : " << left << "  right : " << right << endl; 
        }
        if (left > right) return {-1, -1};
        return {left, right};
    }
};
