#include "HttpServer.hpp"
#include "Log.hpp"
#include <memory>

void Usage(char *args)
{
  std::cout << "Usage\n\t" << args << " server.port" << std::endl;
}

// int main(){
//   LOG(INFO, "debug project");
//   return 0;
// }

int main(int argc, char *argv[])
{
  if (argc != 2)
  {
    Usage(argv[0]);
    exit(4);
  }
  int port = atoi(argv[1]);
  std::shared_ptr<HttpServer> http_server(new HttpServer(port));

  http_server->InitServer();
  http_server->Loop();

  while (true)
  {
  }
}
