#pragma once 
#include <fcntl.h>
#include <iostream>
#include <unistd.h>

class Util {
    public:
        static bool SetNonBlock(int fd) {
            // 获取状态标识位
            int fl = fcntl(fd, F_GETFL);
            if (fl < 0) return false;
            fcntl(fd, F_SETFL, fl | O_NONBLOCK);
            return true;
        }
};