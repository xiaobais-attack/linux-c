class Solution {
public:
    // 3 4 5 1 2
    int findMin(vector<int>& nums) {
        int left = 0, right = nums.size() - 1;
        while (left <= right) {
            if (nums[left] < nums[right]) break;
            int mid = (left + right) / 2;
            if (nums[mid] > nums[right]) 
                left = mid + 1;
            else if (nums[mid] < nums[right]) 
                right = mid;
            else 
                right--;
        }
        return nums[left];
    }
};
