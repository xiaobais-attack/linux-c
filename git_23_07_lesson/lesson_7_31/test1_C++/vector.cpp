#include "vector.h"

void clx::test_vector1()
  {
    vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    v.push_back(4);
    v.push_back(5);
    vector<int>::iterator it = v.begin();
    while (it != v.end())
    {
      cout << *it << " ";
      it++;
    }
    cout << endl;
  }
