#include <stdio.h>


int main()
{
  double sum = 0;
  int i = 0;
  for(i = 1; i < 101; i++)
  {
    sum += 1.0 / i;
  }
  printf("%.2lf\n", sum);
  return 0;
}
