#include "game.h"

void Menu()
{
  printf("\n");
  printf("welcome to the three chess game\n");
  printf("-------------------------------\n");
  printf("----1.Start         2.out------\n");
  printf("-------------------------------\n");
  printf("\n");
}
void InitBoard(char board[ROW][COL], size_t row, size_t col)
{
  for (size_t i = 0; i < row; i++)
  {
    for (size_t j = 0; j < col; j++)
    {
      board[i][j] = ' ';
    }
  }
}
void DisplayBoard(char board[ROW][COL], size_t row, size_t col)
{
  for (size_t i = 0; i < row; i++)
  {
    printf("          ");
    for (size_t j = 0; j < col; j++)
    {
      printf(" %c ", board[i][j]);
      if (j < col -1)
      {
        printf("|");
      }
    }
    printf("\n");
    if (i < row -1)
    {
      printf("          ");
      printf("-----------\n");
    }
  }
  printf("\n\n");
}
size_t IsFull(char board[ROW][COL], size_t row, size_t col)
{
  if (board[row][col] == ' ')
  {
    return 1;
  }
  else 
  {
    return 0;
  }
}
void PlayerMove(char board[ROW][COL], size_t row, size_t col)
{
  size_t my_row, my_col;
  printf("请先输入行再输入列:>");
  scanf("%lu%lu", &my_row, &my_col);
  while (my_row > row || my_col > col || !IsFull(board, my_row, my_col))
  {
    printf("错误位置或位置已满，请重新输入\n");
    printf("请先输入行再输入列:>");
    scanf("%lu%lu", &my_row, &my_col);
  }
  board[my_row][my_col] = '*';
  DisplayBoard(board, row, col);
  Celebrate(board, row, col);
}

void ComputerMove(char board[ROW][COL], size_t row, size_t col)
{
  size_t c_row, c_col;
  c_row = rand() % 3;
  c_col = rand() % 3;
  while (!IsFull(board, c_row, c_col))
  {
    c_row = rand() % 3;
    c_col = rand() % 3;
  }
  board[c_row][c_col] = '@';
  DisplayBoard(board, row, col);
}
char IsWin(char board[ROW][COL], size_t row, size_t col)
{
  for (size_t i = 0; i < row; i++)
  {
    size_t count_p = 0;
    size_t count_c = 0;
    for (size_t j = 0; j < col; j++)
    {
      if (board[i][j] == '*')
      {
        count_p++;
      }
      if (board[i][j] == '@')
      {
        count_c++;
      }
    }
    if (count_p == 3)
    {
      return 'p';
    }
    if (count_c == 3)
    {
      return 'c';
    }

  }

  for (size_t i = 0; i < row; i++)
  {
    size_t count_p = 0;
    size_t count_c = 0;
    for (size_t j = 0; j < col; j++)
    {
      if (board[j][i] == '*')
      {
        count_p++;
      }
      if (board[j][i] == '@')
      {
        count_c++;
      }
    }
    if (count_p == 3)
    {
      return 'p';
    }
    if (count_c == 3)
    {
      return 'c';
    }

  }

  if (board[0][0] == '*' && board[1][1] == '*' && board[2][2] == '*')
    return 'p';
  if (board[0][2] == '*' && board[1][1] == '*' && board[2][0] == '*')
    return 'p';
  if (board[0][0] == '@' && board[1][1] == '@' && board[2][2] == '@')
    return 'c';
  if (board[0][2] == '*' && board[1][1] == '*' && board[2][0] == '*')
    return 'c';
  size_t count = 0;
  for (size_t i = 0; i < row; i++)
  {
    for(size_t j = 0; j < col; j++)
    {
      if (board[i][j] != ' ')
      {
        count++;
      }
    }
  }
  if (count == (row * col))
  {
    return 'o';
  }
  return 'v';
}


size_t Celebrate(char board[ROW][COL], size_t row, size_t col)
{
  char ch = IsWin(board, row, col);
  if (ch == 'p')
  {
    printf("Player win\n");
    return 0;
  }
  if (ch == 'c')
  {
    printf("Computer win\n");
    return 0;
  }
  if (ch == 'o')
  {
    printf("if ends in a draw\n");
    return 0;
  }
    return 1;
}













































