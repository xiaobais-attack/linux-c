#include <iostream>
#include <string>
using namespace std;

class Date
{
  public:
    Date(size_t year = 0, size_t month = 1, size_t day = 1)
      :_year(year)
      ,_month(month)
      ,_day(day)
  {}
  private:
    size_t _year;
    size_t _month;
    size_t _day;

};

template<class T1, class T2>
class Data 
{
  public:
    Data()
    {
      cout << "Data<T1, T2>" << endl;
    }
  private:
    T1 _d1;
    T2 _d2;
};

template<class T>
class Data<T, char>
{
  public:
    Data()
    {
      cout << "Data<T, char>" << endl;
    }
  private:
    T _d1;
    char _d2;
};

template<class T1, class T2>
class Data<T1*, T2*>
{
  public:
    Data()
    {
      cout << "Data<T1*, T2*>" << endl;
    }
  private:
    T1* _d1;
    T1* _d2;
};

template<class T>
bool Less(T left, T right)
{
  return left < right;
}

template<>
bool Less<int*>(int* left, int* right)
{
  return *left < *right;
}

template<size_t N>
class Te
{
  public:
    Te()
    {
      cout << "Te<size_t N>" << endl;
    }
};
template<>
class Te<1000>
{
  public:
    Te()
    {
      cout << "Te<1000>" << endl;
    }
};
int main()
{
  Te<1> t1;
  Te<1000> t2;
  return 0;
}

//int main()
//{
//  Data<int, char> d1;
//  Data<int, int> d2;
//  Data<int*, char*> d3;
//  return 0;
//}

//int main()
//{
//  int a = 10;
//  int b = 20; 
//  cout << Less(a, b) << endl;
//  cout << Less(&a, &b) << endl;
//
//  return 0;
//}
