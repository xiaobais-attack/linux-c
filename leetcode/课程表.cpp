class Solution {
public:
    vector<vector<int>> edges;
    vector<int> inedg;
    bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
        edges.resize(numCourses);
        inedg.resize(numCourses);

        for (int i = 0; i < numCourses; i++) {
            for (const auto& ele : prerequisites) {
                edges[ele[1]].push_back(ele[0]);
                inedg[ele[0]]++;
            }
        }

        queue<int> q;
        for (int i = 0; i < inedg.size(); i++) {
            if (inedg[i] == 0) q.push(i);
        }

        int visited = 0;
        while (!q.empty()) {
            int course = q.front(); q.pop();
            visited++;

            for (const auto ele : edges[course]) {
                inedg[ele]--;
                if (inedg[ele] == 0) {
                    q.push(ele);
                }
            }
        }

        if (visited == numCourses) return true;
        else return false;
    }
};
