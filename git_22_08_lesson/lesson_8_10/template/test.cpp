#include <iostream>
#include <vector>
#include <string>
#include <string.h>
using namespace std;

template<class T1, class T2>              //类模板
class Date
{
  public:
    Date()
    {
      cout << "Date<T1, T2>" << endl;
    }
};

template<> 
class Date<int, int>                    //全特化模板
{
  public:
    Date()
    {
      cout << "Date<int, int>" << endl;
    }
};
template<class T1>                     //偏特化模板 第二个模板参数是int
class Date<T1, int>
{
  public:
    Date()
    {
      cout << "Date<T1, int>" << endl;
    }
};
template<class T1, class T2>           //偏特化模板 模板参数是指针
class Date<T1*, T2*>
{
  public:
    Date()
    {
      cout << "Date<T1*, T2*>" << endl;
    }
};

template<class T>
bool IsEqual(const T& left, const T& right)
{
  return left == right;
}

bool IsEqual(const char*&left, const char* const& right)
{
  return strcmp(left, right);
}

template<>
bool IsEqual<const char*>(const char* const &left, const char* const &right)
{
  return strcmp(left, right);
}
void test1()
{
  Date<int, double> d1;
  Date<int, int> d2;
  Date <double, int> d3;
  Date<int*, int*> d4;

}

int main()
{
  test1();
  return 0;
}
