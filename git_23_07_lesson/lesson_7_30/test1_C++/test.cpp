#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>
using namespace std;

template<class T>
void print_vector(const vector<T>& v)
{
  vector<T>::const_iterator it1 = v.begin();
  while (it1 != v.end())
  {
    cout << *it1 << " ";
    it++;
  }
  cout << endl;
}

void print_vector(const vector<int>& v)
{
  vector<int>::const_iterator it = v.begin();
  while (it != v.end())
  {
    cout << *it << " ";
    it++;
  }
  cout << endl;
}
void print_vector(const vector<char>& v)
{
  vector<char>::const_iterator it = v.begin();
  while (it != v.end())
  {
    cout << *it << " ";
    it++;
  }
  cout << endl;
}

void print_vector(const vector<string>& v)
{
  vector<string>::const_iterator it = v.begin();
  while (it != v.end())
  {
    cout << *it << " ";
    it++;
  }
  cout << endl;
}
void vector_test2()
{
  size_t sz;
  vector<int> v1;
  sz = v1.capacity();
  cout<< "making v1 grow.. \n";
  for (size_t i = 0; i < 100; i++)
  {
    v1.push_back(i);
    if (sz != v1.capacity())
    {
      sz = v1.capacity();
      cout << "capacity changed:" << sz << endl;
    }
  }
}
int main()
{
  vector_test2();
  //vector<int> v1;
  //v1.push_back(1);
  //v1.push_back(2);
  //v1.push_back(3);
  //v1.push_back(4);
  //for (size_t i = 0; i < v1.size(); i++)
  //{
  //  cout << v1[i] << " ";
  //}
  //cout << endl;

  //vector<int>::iterator it1 = v1.begin();
  //while (it1 != v1.end())
  //{
  //  cout << *it1 << " ";
  //  it1++;
  //}
  //cout << endl;

  //for(auto e : v1)
  //{
  //  cout << e << " ";
  //}
  //cout << endl;


  //vector<int> v2(10, 0);
  //vector<int> v3(v2.begin(), v2.end());
  //string str1 = "hello world ";
  //vector<char> v4(str1.begin(), str1.end());
  //vector<string> v5;
  //v5.push_back(str1);
  //v5.push_back("hello");
  //v5.push_back("Linux");
  //print_vector(v5);
  //print_vector(v4);
  //








  


}
