#include "Reactor.hpp"
#include "Sock.hpp"
#include "Accepter.hpp"
#include "Util.hpp"

void Usage(char* proc){
    std::cout << "Usage: \n\t" << proc << " port" << std::endl; 
}

int main(int argc, char* argv[]){
    if (argc != 2){
        Usage(argv[0]);
        return 1;
    }
    // 1.创建listen_sock ,进行监听
    int listen_sock = Sock::Socket();
    SetNonBlock(listen_sock);
    uint16_t port = (uint16_t)atoi(argv[1]);
    Sock::Bind(listen_sock, port);
    Sock::Listen(listen_sock);

    // 2.创建Reactor对象
    Reactor *R = new Reactor();
    R->InitReactor();

    // 3.给Reactor反应堆中加入反应物
    // 3.1 创建反应物
    Event *evp = new Event();
    evp->sock = listen_sock;
    evp->R = R;
    evp->RegisterCallback(Accepter, nullptr, nullptr);
    // 3.2 将反应物放到反应堆中
    R->InsertEvent(evp, EPOLLIN | EPOLLET); // 使用ET模式（边缘触发）模式进行处理

    // 4.开始进行事件派发
    for( ; ; ){
        R->Dispatcher(1000);
    }

}