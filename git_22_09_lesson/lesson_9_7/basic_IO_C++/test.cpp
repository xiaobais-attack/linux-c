#include <iostream>
#include <string>
#include <cstdio>
#include <assert.h>
#include <fstream>
using namespace std;


struct ServerInfo 
{
  char _ip[32];
  int _port; //端口
};

class ConfigManager
{
  public:
    ConfigManager(const string& filename)
      :_filename(filename)
    {}
    void bin_write(const ServerInfo& info)
    {
      ofstream ofs(_filename.c_str(), ios_base::out | ios_base::binary);
      ofs.write((const char*)&info, sizeof(ServerInfo));
    }
    void bin_read(ServerInfo& info)
    {
      ifstream ifs(_filename.c_str(), ios_base::in | ios_base::binary);
      ifs.read((char*)&info, sizeof(ServerInfo));

    }

  private:
    string _filename;

};











//struct ServerInfo 
//{
//  char _ip[32];
//  int _port; //端口
//};

////C语言二进制读写
//void test_bin_fwrite()   
//{
//  FILE* fout = fopen("test.file_bin", "w");
//  assert(fout);
//  ServerInfo info1 = {"128.0.0", 1};
//  fwrite(&info1, sizeof(ServerInfo), 1, fout);
//  fclose(fout);
//}
//
//void test_bin_fread()
//{
//  FILE* fin = fopen("test.file_bin", "r");
//  assert(fin);
//  ServerInfo info2;
//  fread(&info2, sizeof(ServerInfo), 1, fin);
//  fclose(fin);
//  printf("%s: %d\n", info2._ip, info2._port);
//  //fread()
//}
//
////文本读写
//void test_fprintf()
//{
//  FILE* fout = fopen("test.file", "w");
//  assert(fout);
//  ServerInfo info1 = {"122.0.0", 2};
//  fprintf(fout, "%s %d", info1._ip, info1._port);
//  fclose(fout);
//}
//void test_fscanf()
//{
//  FILE* fin = fopen("test.file", "r");
//  assert(fin);
//  ServerInfo info2;
//  fscanf(fin, "%s%d", info2._ip, &info2._port);
//  fclose(fin);
//  printf("%s %d\n", info2._ip, info2._port);
//
//}
//
//int main()
//{
//  //test_bin_fwrite();
//  //test_bin_fread(); 
//  test_fprintf();
//  test_fscanf();
//  return 0;
//}
//
//class Data
//{
//  public:
//    Data(size_t a, char b)
//      :_a(a)
//      ,_b(b)
//  {}
//    operator bool()
//    {
//      return _a != 0;
//    }
//    size_t _a;
//    char _b;
//};
//int main()
//{
//  Data d1(5, 0);
//  while (d1)
//  {
//    d1._a--;
//    cout << d1._a << " ";
//  }
//  cout << endl;
//  return 0;
//}
//int main()
//{
//  string str;
//  while (cin >> str)
//  {
//    cout << str << endl;
//  }
//  return 0;
//}
//
//
//
//
//
