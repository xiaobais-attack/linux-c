#pragma once
#include <iostream>
#include <string>
#include <string.h>
#include <vector>
#include <algorithm>
using namespace std;

namespace clx
{
  template<class T>
  class vector 
  {
    public:
      typedef T* iterator;
      vector()
      :_start(nullptr)
      ,_finish(nullptr)
      ,_endofstorage(nullptr)
    {}
      iterator begin()
      {
        return _start;
      }
      iterator end()
      {
        return _finish;
      }
      size_t capacity()
      {
        return _endofstorage - _start;
      }
      size_t size()
      {
        return _finish - _start;
      }
      void reserve(size_t n)
      {
        if (n > capacity())
        {
          T* tmp = new T[n];
          size_t sz = size();
          if (_start)
          {
            memcpy(tmp, _start, sz * sizeof(T));
            delete[] _start;
          }
          _start = tmp;
          _finish = _start + sz;
          _endofstorage = _start + n;
        }
      }
      void push_back(const T& x)
      {
        if (_finish == _endofstorage)
        {
          size_t newcapacity = capacity() == 0 ? 4 : 2 * capacity();
          reserve(newcapacity);
        }
        *_finish = x;
        ++_finish;

      }

    private:
      iterator _start;
      iterator _finish;
      iterator _endofstorage;
  };
  void test_vector1();

}

