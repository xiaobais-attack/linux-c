class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        unordered_map<string, vector<string>> book;
        for (int i = 0; i < strs.size(); i++) {
            string strKey = strs[i];
            sort(strKey.begin(), strKey.end());
            book[strKey].push_back(strs[i]);
        }
        vector<vector<string>> result;
        for (auto ele : book) {
            result.push_back(ele.second);
        }
        return result;
    }
};
