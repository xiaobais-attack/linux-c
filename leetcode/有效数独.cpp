class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        unordered_map<int, bitset<256>> row_map;
        unordered_map<int, bitset<256>> col_map;
        unordered_map<int, bitset<256>> matrix_map;

        for (int i = 0; i < 9; i++){
            for (int j = 0; j < 9; j++){
                if (board[i][j] <= '0'|| board[i][j] > '9') continue;
                int row_idx = i / 3;
                int col_idx = j / 3;
                int flag = row_idx * 10 + col_idx;
                if (row_map[i].test(board[i][j]))
                    return false;
                else 
                    row_map[i].set(board[i][j]);

                if (col_map[j].test(board[i][j]))
                    return false;
                else 
                    col_map[j].set(board[i][j]);

                if (matrix_map[flag].test(board[i][j]))
                    return false;
                else 
                    matrix_map[flag].set(board[i][j]);
            }
        }
        return true;
    }
};
