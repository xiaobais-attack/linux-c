#pragma once 
#include <iostream>
#include <assert.h>
#include <string.h>
using namespace std;

namespace clx
{
  class string 
  {
    public:
      //lterator
      typedef char* iterator;
      typedef const char* const_iterator;
      iterator& begin() {
        return _str;
      }
      iterator end() {
        return _str + _size;
      }
      const_iterator begin() const  {
        return _str;
      }
      const_iterator end() const  {
        return _str + _size;
      }
      void my_swap(string& s);
      //Four default function
      string(const char* s = "");   //default constructs function
      string(const string& s); //copy constructs function
      string& operator=(string s); //Assignment operator overload
      ~string();  //destructor function

      //[] operator overload
      char& operator[](size_t pos);
      char operator[](size_t pos) const;
      //out put size
      size_t size() const;
      size_t size();
      //out put string
      char* c_str();
      char* c_str() const;
      //chang capacity success return 1 fail return 0
      size_t reserve(size_t n);
      //resizes the string to a length of n characters
      size_t resize(size_t n, char ch = '\0');

      //string increase
      string& push_back(char ch);
      string& append(const char* s);
      string& insert(size_t pos, const char* s);
      string& insert(size_t pos, char ch);
      string& operator+=(const char* s);
      string& operator+=(char ch);
      //string delete 
      string& erase(size_t pos = 0, size_t len = npos);
      string& clear();
      //find char or string 
      size_t find(char ch, size_t pos = 0) const;
      size_t find(const char* s, size_t pos = 0) const ;
      size_t rfind(char ch, size_t pos = 0) const;
      size_t rfind(const char* s, size_t pos = 0) const ;
    private:
      char* _str;
      size_t _size;
      size_t _capacity;
      static size_t npos;
  };
  //Logical operator overloading
  bool operator==(const string str1, const string str2);
  bool operator!=(const string str1, const string str2);
  bool operator>=(const string str1, const string str2);
  bool operator<=(const string str1, const string str2);
  bool operator>(const string str1, const string str2);
  bool operator<(const string str1, const string str2);
  //io operator overloading
  ostream& operator<<(ostream& out, const string str);
  istream& operator>>(istream& in , string& str);
  istream& getline(istream& in , string& str);
  void string_test1();
  void string_test2(); 
  void string_test3();
  void string_test4();
  void string_test5();
  void string_test6();
  void string_test7();
}


