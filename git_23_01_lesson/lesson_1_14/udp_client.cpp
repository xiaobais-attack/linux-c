#include <iostream>
#include <string>
#include <cerrno>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

void Usage(std::string proc){
    std::cout << "Usage:\n\t" << proc << " server_ip server_port" << std::endl;
}

int main(int argc, char* argv[]){
    if (argc != 3) {
        Usage(argv[0]);
        return 3;
    }
    //1、创建套接字,打开网络文件
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0){
        std::cerr << "socket error : " << errno << std::endl;
        return 1; 
    }
    //2、客户端也必须显示的bind的吗?
    //a、首先，客户端必须也要有ip和port
    //b、但是，客户端不需要显示bind！一旦显示bind，就必须明确，client要和哪一个port关联
    //client指明端口号，在client一定存在吗？？会不会已经被其他人绑定了呢？？一旦port被占用则无法使用服务
    //server要求port必须明确，并且不变，但client只要有就可以！一般是OS自动给我们bind()
    //当client正常发送数据的时候，OS会自动给你bind，采用随机端口的方式，自动帮你匹配合适端口

    //2.使用服务
    while (true){
        std::string message;
        std::cout << "输入#";
        std::cin >> message;
        //a、你的数据从哪里来
        //b、你要发给谁
        sockaddr_in server;
        server.sin_family = AF_INET;
        server.sin_port = htons(atoi(argv[2]));
        server.sin_addr.s_addr = inet_addr(argv[1]);
        sendto(sock, message.c_str(), message.length(), 0, (struct sockaddr*)&server, sizeof(server));
        struct sockaddr_in tmp;
        socklen_t len = sizeof(tmp);
        char buffer[1024];
        recvfrom(sock, buffer, sizeof(buffer), 0, (struct sockaddr*)&tmp, &len);
        std::cout << "server echo#" << buffer << std::endl;
    }
    return 0;
}
