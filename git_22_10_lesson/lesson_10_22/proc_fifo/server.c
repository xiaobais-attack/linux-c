#include "comm.h"

int main()
{
if (mkfifo(MY_FIFO, 0666) < 0){
    perror("mkfifo error\n");
    exit(1);
  }
  //一旦我们拥有了命名管道，只需要进行文件操作即可
  int fd = open(MY_FIFO, O_RDONLY);
  if (fd < 0) {
    perror("open error\n");
    exit(2);
  }
  //业务逻辑,可以进行对应的读写
  while (1)
  {
    char buffer[64] = {0};
    //即使接收了很多字符串，但是并不会写到文件种
    int s = read(fd, buffer, 64 - 1);
    if (s > 0){
      buffer[s] = 0;
      if (strcmp(buffer, "show") == 0){
        if (fork() == 0){
          execl("/usr/bin/ls", "ls", "-a", "-l", "-i", NULL);
          exit(3);
        }
        waitpid(-1, NULL, 0);
      }
      else if (strcmp(buffer, "run") == 0){
        if (fork() == 0){
          execl("/usr/bin/sl", "sl", NULL);
          exit(4);
        }
        waitpid(-1, NULL, 0);
      }
      else {
        printf("client say %s\n", buffer);
      }
    }
    else if (s == 0){
      printf("client quit ..\n");
      sleep(5);
    }
    else {
      perror("error\n");
    }
  }


  close(fd);
  return 0;
}
