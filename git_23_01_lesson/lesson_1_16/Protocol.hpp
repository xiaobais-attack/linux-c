#pragma once 
#pragma once
#include <iostream>
#include <string>
#include <cstring>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <jsoncpp/json/json.h>
using namespace std;

typedef struct request{
    int x;
    int y;
    char op;
}request_t;

// response format 相应格式
typedef struct response{
    int sign;
    int result; 
}response_t;

std::string SerializeRequest(const request_t &req){
    Json::Value root;
    root["datax"] = req.x;
    root["datay"] = req.y;
    root["operator"] = req.op;

    Json::FastWriter writer;
    std::string json_string = writer.write(root);
    return json_string;
}

void DeserializeRequest(const std::string &json_string, request_t &out){
    Json::Reader reader;
    
    Json::Value root;
    reader.parse(json_string, root);
    out.x = root["datax"].asInt();
    out.y = root["datay"].asInt();
    out.op = root["operator"].asUInt();
}

std::string SerializeResponse(const response_t &resp){
    Json::Value root;
    root["sign"] = resp.sign;
    root["result"] = resp.result;

    Json::FastWriter writer;
    std::string json_string = writer.write(root);
    return json_string;
}

void DeserializeResponse(const std::string &json_string, response_t &out){
    Json::Reader reader;
    
    Json::Value root;
    reader.parse(json_string, root);
    out.sign = root["sign"].asInt();
    out.result = root["result"].asInt();
}
