#pragma once 
#include <iostream>
#include <vector>
using namespace std;

template<size_t N>
class my_bitset
{
  public:
  my_bitset() {v.resize(N / 8 + 1, 0);}
  my_bitset& set(size_t pos);
  my_bitset& reset (size_t pos);
  bool test(size_t pos);
  size_t size();
  private:
    vector<char> v;
};

template<size_t N>
my_bitset<N>& my_bitset<N>::set(size_t pos)
{
  size_t  index = pos / 8;
  size_t bit_index = pos % 8;
  v[index] = v[index] | (1 << bit_index);
  return *this;
}

template<size_t N>
my_bitset<N>& my_bitset<N>::reset(size_t pos)
{
  size_t index = pos / 8;
  size_t bit_index = pos % 8;
  v[index] = v[index] & (~(1 << bit_index));
  return *this;
}

template<size_t N>
bool my_bitset<N>::test(size_t pos)
{
  size_t index = pos / 8;
  size_t bit_index = pos % 8;
  return (bool)(v[index] & (1 << bit_index));
}

void my_bitset_test()
{
  my_bitset<1000> bs;
  bs.set(1);
  bs.set(11);
  bs.set(15);
  cout << bs.test(11) << endl;
  cout << bs.test(16) << endl;
}
