#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>

int g_unval;
int g_val = 100;
int main(int argc, char* argv, char* envp[])
{
  printf("code addr: %p\n", main);//代码区
  char* str = "hello world";

  printf("read only addr: %p\n",str);//常量区 
  printf("init addr: %p\n",&g_val);//初始化变量
  printf("uninit addr: %p\n",&g_unval);//未初识化变量
  int* p =(int*)malloc(sizeof(int));
  printf("heap addr: %p\n",p);//堆区
  printf("stack addr: %p\n",&str);//栈区
  printf("stack addr: %p\n",&p);//栈区
  
  for(int i = 0; i < argc; i++)
  {
    printf("args addr: %p\n", argv[i]);//命令行参数
  }
  int i = 0;
  while (envp[i]){
    printf("env addr: %p\n", envp[i]);//环境变量
    i++;
  }
  return 0;
}
//
//int main()
//{
//  pintf("%s\n", getenv("PATH"));
//}

//int main()
//{
//  extern char **environ;
//  for (int i = 0; environ[i] != 0; i++)
//  {
//    printf("%s\n", environ[i]);
//  }
//}




//int main(int argc, char* argv[], char* envp[])
//{
//  int i = 0;
//  while (envp[i])
//  {
//    printf("envp[%d]:%s\n", i, envp[i]);
//    i++;
//  }
//}
//int main(int argc, char *argv[], char *envp[])
//{
//  printf("%d\n", argc);
//  if (argc == 2)
//  {
//    if (strcmp(argv[1], "a") == 0)
//    {
//      printf("Hello world\n");
//    }
//    else if (strcmp(argv[1], "b") == 0)
//    {
//      printf("Hello Linux\n");
//    }
//    else 
//    {
//      printf("Hello default\n");
//    }
//  }
//
//}
