class StackOfPlates {
public:
    vector<stack<int>> v;
    int capacity;
    StackOfPlates(int cap) {
        capacity = cap;
    }

    void push(int val) {
        if (capacity == 0) return;
        if (v.empty()) {
            v.push_back(stack<int>());
            v[0].push(val);
        }
        else {
            int index = v.size() - 1;
            if (v[index].size() == capacity) {
                v.push_back(stack<int>());
                index++;
            }
            v[index].push(val);
        }
    }
    
    int pop() {
        if (v.empty()) return -1;
        int index = v.size() - 1;
        int res = v[index].top();
        if (v[index].size() == 1) {
            v.pop_back();
        }
        else {
            v[index].pop();
        }
        return res;
    }
    
    int popAt(int index) {
        if (v.empty() || index >= v.size()) {
            return -1;
        }
        int res = v[index].top();
        if (v[index].size() == 1) {
            v.erase(v.begin() + index);
        }
        else {
            v[index].pop();
        }
        return res;
    }
};

/**
 * Your StackOfPlates object will be instantiated and called as such:
 * StackOfPlates* obj = new StackOfPlates(cap);
 * obj->push(val);
 * int param_2 = obj->pop();
 * int param_3 = obj->popAt(index);
 */
