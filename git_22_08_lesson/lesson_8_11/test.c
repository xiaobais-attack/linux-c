RT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

void Rotate_left(char* str, int count)
{
	int len = strlen(str);
	for (int i = 0; i < count; i++)
	{
		int end = len;
		int cur = 1;
		char tmp = str[0];
		while (cur != end)
		{
			str[cur - 1] = str[cur];
			cur++;
		}
		str[len - 1] = tmp;
	}
}
void Rotate_right(char* str, int count)
{
	int len = strlen(str);
	for (int i = 0; i < count; i++)
	{
		int end = -1;
		int cur = len - 2;
		char tmp = str[len -1];
		while (cur != end)
		{
			str[cur + 1] = str[cur];
			cur--;
		}
		str[0] = tmp;
	}
}

int IsRotate(char* str1, char* str2)
{
	int len1 = strlen(str1);
	for (int i = 0; i < len1; i++)
	{
		Rotate_left(str1, 1);
		if (strcmp(str1, str2) == 0)
		{
			return 1;
		}
	}
	for (int j = 0; j < len1; j++)
	{
		Rotate_right(str2, 1);
		if (strcmp(str1, str2) == 0)
		{
			return 1;
		}
	}
	return 0;
}
int main()
{
	char str1[] = "AABCD";
	char str2[] = "BCDAA";
	printf("%d", IsRotate(str1, str2));
}









//#define ROW 100
//#define COL 100
//int* Search(int arr[ROW][COL], int val, int row, int col)
//{
//	int cur_row = 0;
//	int cur_col = col - 1;
//	for (int i = 0; i < row + col; i++)
//	{
//		if (cur_row == row){
//			return NULL;
//		}
//		if (cur_col == -1){
//			return NULL;
//		}
//		if (val > arr[cur_row][cur_col]){
//			cur_row++;
//		}
//		else if (val < arr[cur_row][cur_col]){
//			cur_col--;
//		}
//		else{
//			return &arr[cur_row][cur_col];
//		}
//	}
//}
//int main(){
//	int row, col, val;
//	scanf("%d%d%d", &row, &col, &val);
//	int arr[ROW][COL] = {0};
//	for (int j = 0; j < col; j++){
//		for (int i = 0; i < row; i++){
//			arr[i][j] = j * row + i;
//		}
//	}
//	for (int i = 0; i < row; i++){
//		for (int j = 0; j < col; j++){
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	int* p_val = Search(arr, val, row, col);
//	if (p_val){
//		printf("存在%d", *p_val);
//	}
//	else{
//		printf("矩阵中不存在%d\n", val);
//	}
//	return 0;
//}
