class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& temperatures) {
        if (temperatures.empty()) return {};
        stack<pair<int, int>> st;
        vector<int> result(temperatures.size(), 0);
        int end = 0;
        for (int i = 0; i < temperatures.size(); i++) {
            if (st.empty()) {
                st.push({temperatures[i], i});
                continue;
            }

            while (!st.empty() && temperatures[i] > st.top().first) {
                pair<int, int> tmp = st.top();
                st.pop();
                result[tmp.second] = i - tmp.second; 
                end = max(end, tmp.second);
            }
            st.push({temperatures[i], i});
        }
        for (int i = end + 1; i < temperatures.size(); i++) {
            result[i] = 0;
        }
        return result;

    }
};
