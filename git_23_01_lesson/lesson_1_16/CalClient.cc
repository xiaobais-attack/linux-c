#include "Protocol.hpp"
#include "Sock.hpp"

void Usage(std::string proc){
  std::cout << "Usage: " << "\n\t" << proc << "server_ip server port" << std::endl;
}

int main(int argc, char* argv[]){
  if (argc < 3){
    Usage(argv[0]);
    exit(1);
  }
  int sock = Sock::Socket();
  Sock::Connect(sock, argv[1], atoi(argv[2]));
  request_t req;
  cout << "Please Enter Date One# ";
  cin >> req.x;
  cout << "Please Enter Date Two# ";
  cin >> req.y;
  cout << "Please Enter Operator";
  cin >> req.op;
  std::string json_string = SerializeRequest(req);
  write(sock, json_string.c_str(), json_string.size());
  // write(sock, &req, sizeof(req));
  response_t resp;
  char buffer[1024] = {0};
  ssize_t s = read(sock, buffer, sizeof(buffer) - 1);
  buffer[s] = 0;
  DeserializeResponse(buffer, resp);
  if (s > 0){
    if (resp.sign == 1){
      std::cout << "除零错误" << std::endl;
    }
    else if (resp.sign == 3){
      std::cout << "非法运算符" << std::endl;
    }
    else {
      std::cout << "result = " << resp.result << std::endl;
    }
  }
  return 0;
}
