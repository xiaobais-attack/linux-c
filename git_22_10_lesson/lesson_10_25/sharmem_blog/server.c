#include "comm.h"

int main()
{
  //成功返回key值，失败返回-1
  key_t key = ftok(SHM_PATH, SHM_ID);
  if (key < 0){
    perror("ftok failed\n");
    exit(1);
  }

  int shmid = shmget(key, 1024*4, IPC_CREAT | IPC_EXCL | 0666);
  if (shmid < 0){
    perror("shmget failed\n");
    exit(2);
  }

  char* my_shm = (char*)shmat(shmid, NULL, 0);

  //读取共享内存中的信息
  while (1){
    sleep(1);
    printf("%s\n", my_shm);
  }
  shmdt(my_shm);
  //系统调用删除shmid(共享内存标识符)
  shmctl(shmid, IPC_RMID, NULL);

  return 0;
}
