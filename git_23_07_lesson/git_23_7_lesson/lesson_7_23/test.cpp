ass Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        if (nums.empty()) return 0;
        int base = nums[0];
        int k = 1;
        set<int> book;
        book.insert(base);
        for (int i = 0; i < nums.size(); i++){
            if (nums[i] > base){
                base = nums[i];
                book.insert(base);
                k++;
            }
        }
        int cur = 0;
        for (auto ele : book){
            nums[cur] = ele; 
            cur++;
        }
        return k;
    }
}
