#include "comm.h"

const uint16_t port = 6066;

int main()
{
  int sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock < 0)
  {
    std::cerr << "socket create failed" << errno << std::endl;
    return 1;
  }

  struct sockaddr_in local;
  local.sin_family = AF_INET;
  local.sin_port = htons(port);
  // local.sin_addr.s_addr = inet_addr("11.111.11.111");
  local.sin_addr.s_addr = INADDR_ANY;

  if (bind(sock, (struct sockaddr*)&local, sizeof(local)) < 0){
    std::cerr <<"bind failed" << errno << std::endl;
    return 2;
  }
  // 提供服务
  bool quit = false;
  while (!quit)
  {
    char buffer[1024] = {0};
    struct sockaddr_in src_addr;
    socklen_t len = sizeof(src_addr);
    recvfrom(sock, buffer, sizeof(buffer) - 1, 0, (struct sockaddr *)&src_addr, &len);
    std::cout << "client say:" << buffer << std::endl;
    std::string echo_hello = "hello";
    sendto(sock, echo_hello.c_str(), sizeof(echo_hello.c_str()) - 1, 0, (struct sockaddr *)&src_addr, len);
  }
  return 0;
}
