class Trie {
public:
    vector<Trie*> children;
    bool isEnd;
public:
    Trie() :children(26), isEnd(false) {}
    
    void insert(string word) {
        Trie* node = this;
        for (char ch : word) {
            int index = ch - 'a';
            if (node->children[index] == nullptr) 
                node->children[index] = new Trie();
            node = node->children[index];
        }
        node->isEnd = true;
    }
    
    bool search(string word) {
        Trie* node = this;
        for (char ch : word) {
            int index = ch - 'a';
            if (node->children[index] == nullptr) return false;;
            node = node->children[index];
        }
        if (node->isEnd == false) return false;
        return true;
    }
    
    bool startsWith(string prefix) {
        Trie* node = this;
        for (char ch : prefix) {
            int index = ch - 'a';
            if (node->children[index] == nullptr) {
                return false;
            }
            node = node->children[index];
        }
        return true;
    }
};

/**
 * Your Trie object will be instantiated and called as such:
 * Trie* obj = new Trie();
 * obj->insert(word);
 * bool param_2 = obj->search(word);
 * bool param_3 = obj->startsWith(prefix);
 */
