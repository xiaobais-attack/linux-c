class Solution(object):
    def maxSubArray(self, nums):
        if len(nums) == 0: return 0
        result = nums[0]
        cur = 0
        for i in range(len(nums)):
            cur = max(nums[i], cur + nums[i])
            result = max(result, cur)
        return result


