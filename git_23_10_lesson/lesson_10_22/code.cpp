#include <iostream>
#include <vector>
#include <queue>
using namespace std;

class Solution {
public:
    bool lemonadeChange(vector<int>& bills) {
        vector<int> nums(3, 0);
        for (size_t i = 0; i < bills.size(); i++) {
            if (bills[i] == 5) 
                nums[0]++;
            else if (bills[i] == 10) {
                if (nums[0] == 0) 
                    return false;
                else 
                    nums[0]--;
                nums[1]++;
            }
            else {
                nums[2]++;
                if (nums[1]) {
                    nums[1]--;
                    if (nums[0]) nums[0]--;
                    else return false;
                }
                else if (nums[0] >= 3) nums[0] -= 3;
                else return false;
            }
        }
        return true;
    }
    int halveArray(vector<int>& nums) {
        priority_queue<double, vector<double>, less<double>> que;
        double sum = 0;
        for (size_t i = 0; i < nums.size(); i++) {
            que.push((double)nums[i]);
            sum += nums[i];
        }
        double target = sum / 2;
        int count = 0;
        while (sum > target) {
            double top = que.top();
            que.pop();
            sum -= top / 2;
            que.push(top / 2);
            count++;
        }
        return count;

    }
};
