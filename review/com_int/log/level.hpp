#pragma

#include <iostream>
namespace clx {
    /*
        1、设置日志等级
        2、转换日志等级到字符串
    */
    class LogLevel {
        public:

        enum value {
            UNKNOW = 0,
            INFO,
            DEBUG,
            WARRING,
            ERROR,
            FATAL,
            OFF
        };

        static const char* toString(int level) {
            switch(level) {
                #define TOSTRING(name) #name
                case clx::LogLevel::value::UNKNOW : return TOSTRING(UNKNOW);
                case clx::LogLevel::value::INFO   : return TOSTRING(INFO);
                case clx::LogLevel::value::DEBUG  : return TOSTRING(DEBUG);
                case clx::LogLevel::value::WARRING: return TOSTRING(WARRING);
                case clx::LogLevel::value::ERROR  : return TOSTRING(ERROR);
                case clx::LogLevel::value::FATAL  : return TOSTRING(FATAL);
                case clx::LogLevel::value::OFF    : return TOSTRING(OFF);
                #undef TOSTRING
            }
            return "UNKNOW";
        }
    };
}