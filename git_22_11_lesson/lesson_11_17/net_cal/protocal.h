#pragma once
#include <iostream>
#include <string>
#include <jsoncpp/json/json.h>
struct request_t
{
    int num1;
    int num2;
    char op;
};

struct req_result_t
{
    int exit_code = 0;
    int result = 0;
};

std::string SerializeRequest(const request_t &req)
{
    Json::Value root;
    root["firstnum"] = req.num1;
    root["secondnum"] = req.num2;
    root["operator"] = req.op;

    Json::StyledWriter writer;
    std::string json_string = writer.write(root);
    return json_string;
}

std::string SerializeReqResult(const req_result_t &res)
{
    Json::Value root;
    root["exit_code"] = res.exit_code;
    root["result"] = res.result;

    Json::StyledWriter writer;
    std::string json_string = writer.write(root);
    return json_string;
}

void DeserializeRequest(std::string json_string, request_t *preq)
{
    Json::Reader reader;
    Json::Value root;

    reader.parse(json_string, root);

    preq->num1 = root["firstnum"].asInt();
    preq->num2 = root["secondnum"].asInt();
    preq->op = (char)root["operator"].asInt();
}

void DeserializeReqResult(std::string json_string, req_result_t *pres)
{
    Json::Reader reader;
    Json::Value root;

    reader.parse(json_string, root);

    pres->exit_code = root["exit_code"].asInt();
    pres->result = (char)root["result"].asInt();
}
