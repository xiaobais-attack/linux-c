#include <iostream>
#include <vector>
#include <stack>
using namespace std;


int main() {
    int n = 0; cin >> n;
    vector<int> v(n);
    vector<int> prev(n + 1);
    for (int i = 0; i < n; i++) {
        cin >> v[i];
        prev[i + 1] = prev[i] + v[i];
    }

    vector<int> left(n);
    
    for (int i = 0; i < n; i++) {
        while (!st.empty() && v[st.top()] >= v[i]) 
            st.pop();
        if (st.empty()) left[i] = -1;
        else left[i] = st.top();
        st.push(i);
    }
    st = stack<int>();
    vector<int> right(n);
    for (int i = n - 1; i >= 0; i--) {
        while (!st.empty() && v[st.top()] >= v[i]) 
            st.pop();
        if (st.empty()) right[i] = n;
        else right[i] = st.top();
        st.push(i);
    }

    int result = 0;
    for (int i = 0; i < n; i++) {
        int sum = prev[right[i]] - prev[left[i] + 1];
        // cout << i << " " << left[i] << " " << right[i] <<  " " << sum << endl; 
        result = max(sum * v[i], max(result, v[i] * v[i]));
    }
    cout << result << endl;
    return 0;
}
// 64 位输出请用 printf("%lld")
