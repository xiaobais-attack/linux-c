#include <stdio.h>
#include <stdlib.h>


int main()
{
  int n = 0;
  printf("请输入一个数字:>");
  scanf("%d", &n);
  printf("奇数位：");
  for (int i = 30; i >= 0; i -= 2)
  {
    printf("%d ", (n >> i) & 1);
  }
  printf("\n");
  printf("偶数位: ");
  for (int i = 31; i >= 0; i -= 2)
  {
    printf("%d ", (n >> i) & 1);
  }
  printf("\n");
}

