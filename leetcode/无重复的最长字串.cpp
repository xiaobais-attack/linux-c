class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        // map <key, index> 若 index 在 begin 之前则更新index
        unordered_map<int ,int> book;
        int begin = 0, end = 0, result = 0;
        while (end < s.size()) {
            if (book.find(s[end]) == book.end() || book[s[end]] < begin) {
                book[s[end]] = end;
                end++;
            }
            else {
                begin = book[s[end]] + 1;
                book[s[end]] = end;
                end++;
            }
            result = max(result, end - begin);
            cout << s.substr(begin, result) << endl;
        }
        return result;
    }
}
