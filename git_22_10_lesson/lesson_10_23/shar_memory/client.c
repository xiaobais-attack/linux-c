#include "comm.h"

int main()
{
  key_t key = ftok(PATH_NAME, PROJ_ID);
  if (key < 0){
    perror("ftok error\n");
  }

  sleep(5);
  //获取共享内存
  int shmid = shmget(key, SIZE, IPC_CREAT);
  if (shmid < 0){
    perror("shmid error\n");
    exit(1);
  }

  char* mem = (char*)shmat(shmid, NULL, 0);
  printf("client process attaches success\n");
  //sleep(5);
  int count = 0;
  while (1)
  {
    sleep(1);
    char ch = 'a' + count;
    mem[count] = ch;
    count++;
  }

  shmdt(mem);
  printf("client process detaches success\n");
  //sleep(5);
  return 0;
}
