class Solution {
public:
    string addStrings(string num1, string num2) {
        int cur1 = num1.size() - 1, cur2 = num2.size() - 1;
        
        int flag = 0;
        string result;
        while (cur1 >= 0 || cur2 >= 0) {
            int num = flag;
            if (cur1 >= 0) num += num1[cur1--] - '0';
            if (cur2 >= 0) num += num2[cur2--] - '0';

            flag = 0;
            if (num >= 10) {
                num -= 10;
                flag = 1;
            }
            result = to_string(num) + result;
        }
        if (flag) result = to_string(flag) + result;
        return result;
    }
};
