#pragma once 

#include <iostream>
#include "Log.hpp"
#include "TcpServer.hpp" 
#include "Protocol.hpp"

#define PORT 8081


class HttpServer{
  public:
    HttpServer(int port = PORT)
      :_port(port), _tcp_server(nullptr), _stop(false){}
  public:
    void InitServer() {_tcp_server = TcpServer::GetInstance(_port);}
    void Loop();
    ~HttpServer(){};
    
  private:
    int _port;
    TcpServer* _tcp_server;
    bool _stop;
};

void HttpServer::Loop(){
  LOG(INFO, "loop begin");
  int _listen_sock = _tcp_server->GetSock();
  while (!_stop){
    struct sockaddr_in peer;
    socklen_t peer_len = sizeof(sockaddr_in);
    int sock = accept(_listen_sock, (struct sockaddr*)&peer, &peer_len);
    if (sock < 0){
      continue;
    }
    LOG(INFO, "get a link");
    pthread_t tid;
    int* psock_fp = new int(sock);
    pthread_create(&tid, nullptr, Entrance::HandlerRequest, psock_fp);
    pthread_detach(tid);
  }
}
