#include "UnionFindSet.hpp"


int main() {
	vector<string> v = { "张三", "李四", "王五", "赵六", "田七", "周八", "吴九" };
	clx_datastructure::UnionFindSet<string> ufs(v);
	cout << ufs.getNum() << endl; //7
	
	ufs.unionSet("张三", "李四");
	ufs.unionSet("王五", "赵六");
	cout << ufs.getNum() << endl; //5
	
	ufs.unionSet("张三", "赵六");
	cout << ufs.getNum() << endl; //4

	return 0;
}
