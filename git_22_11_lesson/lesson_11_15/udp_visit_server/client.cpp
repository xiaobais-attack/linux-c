#include "comm.h"

void Usage(std::string proc)
{
  std::cout << "Usage: \n\t" << proc << "server_ip server_prot" << std::endl;
}

int main(int argc, char *argv[])
{
  if (argc != 3)
  {
    Usage(argv[0]);
    return 0;
  }
  int sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock < 0)
  {
    std::cerr << "socket failed" << errno << std::endl;
    return 1;
  }

  struct sockaddr_in server;
  server.sin_family = AF_INET;
  server.sin_port = htons(atoi(argv[2]));
  server.sin_addr.s_addr = INADDR_ANY;

  while (true)
  {
    std::cout << "请输入指令:>";
    char command[1024] = {0};
    fgets(command, sizeof(command), stdin);
    
    sendto(sock, command, strlen(command), 0, (struct sockaddr*)&server, sizeof(server));
    char buffer[1024];
    struct sockaddr_in tmp;
    socklen_t len = sizeof(tmp);
    ssize_t size = recvfrom(sock, buffer, sizeof(buffer), 0, (struct sockaddr *)&tmp, &len);
    buffer[size] = 0;
    std::cout << "server say:" << buffer << std::endl;
  }
  return 0;
}
