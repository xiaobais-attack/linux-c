class Solution {
public:
    vector<string> result;
    string sol;
    void dfs(int n, int l, int r) {
        if (l > n || r > l) return;
        if (l == n && r == n) {
            result.push_back(sol);
            return;
        }
        sol.push_back('(');
        dfs(n, l + 1, r);
        sol.pop_back();

        sol.push_back(')');
        dfs(n, l, r + 1);
        sol.pop_back();
    }
    vector<string> generateParenthesis(int n) {
        dfs(n, 0, 0);
        return result;
    }
};
