#include "game.h"

void test_1()
{
  char board[ROW][COL]; 
  Menu();
  InitBoard(board, ROW, COL);
  DisplayBoard(board, ROW, COL);
  PlayerMove(board, ROW, COL);
  ComputerMove(board, ROW, COL);
  PlayerMove(board, ROW, COL);
  PlayerMove(board, ROW, COL);
  PlayerMove(board, ROW, COL);
}
void game()
{
  char board[ROW][COL];
  InitBoard(board, ROW, COL);
  DisplayBoard(board, ROW, COL);
  while (Celebrate(board, ROW, COL))
  {
    PlayerMove(board, ROW, COL);
    ComputerMove(board, ROW, COL);
  }

}
void game_test()
{
  
  size_t chess = 0;
  do 
  {
    Menu();
    printf("请选择是否开始游戏:>");
    scanf("%lu", &chess);
    if (chess == 0)
    {
      break;
    }
    else if (chess == 1)
    {
      printf("game start\n");
      game();
    }
    else 
    {
      printf("输入错误请重新输入\n");
    }
    
  }while(1);
  
}

int main()
{
  srand((unsigned int)time(NULL));
  game_test();
  return 0;

}
