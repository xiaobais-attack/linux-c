#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>

int main()
{
  pid_t ids[10];
  for (int i = 0; i < 10; i++)
  {
    pid_t id = fork();
    if (id == 0)
    {
      //child
      int count = 10;
      while (count)
      {
        printf("chjild do something!: %d, %d\n", getpid(), getppid());
        sleep(1);
        count--;
      }
      exit(i);
    }
    //father
    ids[i] = id;
  }

  int count = 0;
  while (count < 10)
  {
    int status = 0;
    pid_t ret = waitpid(ids[count], &status, 0);
    if (ret >= 0)
    {
      printf("wait child success!, %d\n", ret);
      if (WIFEXITED(status))
      {
        printf("child exit code : %d\n", WEXITSTATUS(status));
      }
      else 
      {
        printf("child not exit normal!\n");
      }
      //printf("status: %d\n", status);
      //printf("child exit code : %d\n", (status>>8) & 0xFF);
      //printf("child get signal: %d\n", status & 0x7F);
    }
    count++;
  }

  //if (id == 0)
  //{
  //  size_t count = 30;
  //  while (count)
  //  {
  //    printf("I am child.. pid: %d, ppid: %d\n", getpid(), getppid());
  //    sleep(1);
  //    count--;
  //  }
  //  exit(11);
  //}
  //else 
  //{
  //  printf("I am parente.. pid: %d, ppid: %d\n", getpid(), getppid());
  //  //pid_t ret = wait(NULL);
  //  int status = 0;
  //  pid_t ret = waitpid(id, &status, 0);
  //  if (ret >= 0)
  //  {
  //    printf("wait child success!, %d\n", ret);
  //    printf("status: %d\n", status);
  //    printf("child exit  code: %d\n", (status >> 8) & 0xff);
  //    printf("child get signal: %d\n", status & 0x7F);
  //  }
  //  printf("father run ...\n");
  //  sleep(10);
  //}
  return 0;
}
