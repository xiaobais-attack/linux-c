#include <iostream>
#include <set>
using namespace std;

template<class K>
void set_print(set<K>& s)
{
  for (auto e : s)
  {
    cout << e << " ";
  }
  cout << endl;

}
void set_test()
{
  set<int> s;
  //insert 用法
  //insert 可以排序加去重
  s.insert(3);
  s.insert(5);
  s.insert(2);
  s.insert(4);
  s.insert(1);
  s.insert(6);
  //method 1 : iterator(迭代器)
  //set<int>::iterator it = s.begin();
  //while (it != s.end())
  //{
  //  cout << *it << " ";
  //  it++;
  //}
  //cout << endl;
  //method 2 : range for(范围for)
  //for (auto& e : s)
  //{
  //  cout << e << " ";
  //}
  //cout << endl;
  //erase 用法
  //key iterator iterator range
  set_print(s);
  s.erase(2);                    //删除成功返回1,失败返回0 返回值的类型是 unsigned int
  set_print(s);
  s.erase(s.find(4));            //find返回一个迭代器
  set_print(s);
  s.erase(s.find(1), s.find(3)); //find range
  set_print(s);
  //s.erase(s.find(100));//若使用迭代器必须要是有效的，无效会报错
}


void multiset_test()
{
  //multiset 不去重 可排序
  multiset<int> ms;
  ms.insert(5);
  ms.insert(3);
  ms.insert(2);
  ms.insert(6);
  ms.insert(1);
  ms.insert(4);
  ms.insert(5);
  for (auto& e : ms)
  {
    cout << e << " ";
  }
  cout << endl;
  //cout << ms.erase(5) << endl;            //返回值是5在ms中出现的次数
  multiset<int>::iterator pos = ms.find(5); //find 的 val有多个值时, 返回的是中序第一个val的结点的迭代器
  while (pos != ms.end())
  {
    cout <<  *pos << " ";
    pos++;
  }
  cout << endl;
  //方法1
  //删除所有key为5的结点
  //auto start = pos;
  //auto end = pos;
  //while (pos != ms.end() && *pos == 5)
  //{
  //  end = pos;
  //  pos++;
  //}
  //ms.erase(start, end);
  //方法2
  //while (pos != ms.end() && *pos == 5)
  //{
  //  //必须要记录下一个结点的位置，不然erase后pos就变成野指针，无法++
  //  auto next = pos;
  //  next++;
  //  ms.erase(pos);
  //  pos = next;
  //}
}
int main()
{
  set_test();
  //multiset_test();
  return 0;
}
