#include <stdio.h>
#include <stdlib.h>


//比较大小
int int_compar(const void* a, const void* b)
{
	if (*(int*)a > *(int*)b)
	{
		return 1;
	}
	else
	{
		return -1;
	}
}

int char_compar(const void* a, const void* b)
{
	if (*(char*)a > *(char*)b)
	{
		return 1;
	}
	else
	{
		return -1;
	}
}

int double_compar(const void* a, const void* b)
{
	if (*(double*)a > *(double*)b)
	{
		return 1;
	}
	else
	{
		return -1;
	}
}
int main()
{
	int arr1[8] = { 1,3,5,7,2,4,6,8 };
	char arr2[8] = { '1', '3', '5', '7', '2', '4', '6', '8' };
	double arr3[8] = { 1.1 , 2.3, 14, 23.3, 12.2 , 123, 24, 40.2 };
	qsort(arr1, sizeof(arr1) / sizeof(arr1[0]), sizeof(arr1[0]), int_compar);
	qsort(arr2, sizeof(arr2) / sizeof(arr2[0]), sizeof(arr2[0]), char_compar);
	qsort(arr3, sizeof(arr3) / sizeof(arr3[0]), sizeof(arr3[0]), double_compar);
	for (int i = 0; i < sizeof(arr1) / sizeof(arr1[0]); i++)
	{
		printf("%d", arr1[i]);
	}
	printf("\n");
	for (int i = 0; i < sizeof(arr2) / sizeof(arr2[0]); i++)
	{
		printf("%c", arr2[i]);
	}
	printf("\n");
	for (int i = 0; i < sizeof(arr3) / sizeof(arr3[0]); i++)
	{
		printf("%.1llf ", arr3[i]);
	}
	return 0;

}

////交换数据
//void swap(void* a, void* b, int size)
//{
//	for (int i = 0; i < size; i++)
//	{
//		char tmp = 0;
//		tmp = *((char*)a + i);
//		*((char*)a + i) = *((char*)b + i);
//		*((char*)b + i) = tmp;
//	}
//}
////排序
//void my_qsort(void* base, size_t num, size_t size, int (*compar)(const void*, const void*))
//{
//	for (int i = 0; i < num - 1; i++)
//	{
//		int j = 0;
//		int count = 0;
//		for (j = 0; j < num - i - 1; j++)
//		{
//			
//			if (compar((char*)base + j * size, (char*)base + (j + 1) * size) == 1)
//			{
//				swap((char*)base + j * size, (char*)base + (j + 1) * size, size);
//				count++;
//			}
//
//		}
//		//判断是否已经有序
//		if (count == 0)
//		{
//			break;
//		}
//	}
//}


//int main()
//{
//	int arr1[8] = { 1,3,5,7,2,4,6,8 };
//	char arr2[8] = { '1', '3', '5', '7', '2', '4', '6', '8' };
//	double arr3[8] = { 1.1 , 2.3, 14, 23.3, 12.2 , 123, 24, 40.2 };
//	qsort(arr1, sizeof(arr1) / sizeof(arr1[0]), sizeof(arr1[0]), int_compar);
//	qsort(arr2, sizeof(arr2) / sizeof(arr2[0]), sizeof(arr2[0]), char_compar);
//	qsort(arr3, sizeof(arr3) / sizeof(arr3[0]), sizeof(arr3[0]), double_compar);
//	for (int i = 0; i < sizeof(arr1) / sizeof(arr1[0]); i++)
//	{
//		printf("%d", arr1[i]);
//	}
//	printf("\n");
//	for (int i = 0; i < sizeof(arr2) / sizeof(arr2[0]); i++)
//	{
//		printf("%c", arr2[i]);
//	}
//	printf("\n");
//	for (int i = 0; i < sizeof(arr3) / sizeof(arr3[0]); i++)
//	{
//		printf("%.1llf ", arr3[i]);
//	}
//	return 0;
//
//}

