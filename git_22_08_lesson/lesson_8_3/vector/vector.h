#pragma once 
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <assert.h>
using namespace std;


namespace clx
{
  template<class T>
  class vector
  {
    public:
      //iterator
      typedef T* iterator;
      typedef const T* const_iterator;
      iterator begin();
      iterator end();
      const_iterator begin() const;
      const_iterator end() const;


      vector();
      vector(const vector<T>& v); 
      vector<T>& operator=(vector<T> v);
      ~vector();


      //capacity
      size_t size() const;
      size_t capacity() const;
      void reserve(size_t n);
      void resize(size_t n, const T& val = T());
      bool empty() const;

      //modify
      void push_back(const T& val);
      void pop_back();
      void insert(iterator pos, const T& val);
      iterator erase(iterator pos);
      void my_swap(vector<T>& v);

     //see 
      T& operator[](size_t i);
      const T& operator[](size_t i) const;

    private:
      iterator _start;
      iterator _finish;
      iterator _endofstorage;

  };
  void vector_test1();
  void vector_test2();
  void vector_test3();
  template<class T>
  void vector_print(const vector<T>& v);
  
}
