#include <iostream>
#include <string>
using namespace std;

class Date
{
  public:
    Date(size_t year = 0, size_t month = 1, size_t day = 1)
      :_year(year)
      ,_month(month)
      ,_day(day)
  {}
  private:
    size_t _year;
    size_t _month;
    size_t _day;

};


template<class T>
bool Less(T left, T right)
{
  return left < right;
}

//template<>
//bool Less<int*>(int* left, int* right)
//{
//  return *left < *right;
//}

int main()
{
  int a = 10;
  int b = 20; 
  cout << Less(a, b) << endl;
  cout << Less(&a, &b) << endl;

  return 0;
}
