#pragma once
#include <iostream>
#include <queue>
#include <vector>
#include <string>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>



namespace ns_ring_queue
{
    const int g_cap_default = 10;

    template <class T>
    class RingQueue
    {
    private:
        int _capacity;
        std::vector<T> _ring_queue;
        //空格
        sem_t _space_sem;
        //资源
        sem_t _data_sem;
        int _c_step;
        int _p_step;

    public:
        RingQueue(int cap = g_cap_default) : _capacity(cap)
        {
            _c_step = 0;
            _p_step = 0;
            _ring_queue.resize(10);
            sem_init(&_space_sem, 0, 10);
            sem_init(&_data_sem, 0, 0);
        };
        ~RingQueue()
        {
            sem_destroy(&_data_sem);
            sem_destroy(&_space_sem);
            
        }

    public:
        void Push(const T& in)
        {
            sem_wait(&_space_sem);
            _ring_queue[_p_step] = in;
            _p_step++;
            _p_step = _p_step % _capacity;
            sem_post(&_data_sem); 
        }

        void Pop(T *out)
        {
            sem_wait(&_data_sem);
            *out = _ring_queue[_c_step];
            _c_step++;
            _c_step %= _capacity;
            sem_post(&_space_sem);
        }
    };
}
