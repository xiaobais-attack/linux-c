#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>


int main()
{
  ////参数含义：文件名， 传递标志位， 权限
  //int fd = open("./log.txt", O_WRONLY | O_CREAT, 0644);
  //if (fd < 0){
  //  printf("open error\n");
  //}

  //int count = 5;
  //const char* msg = "hello world\n";
  //while (count--){
  //  write(fd, msg, strlen(msg)); //在调用系统接口写入字符串不需要将\0也传入,\0最为字符串结尾的标志只是C的规定
  //}
  //close(fd);
  
  //int fd = open("./log.txt", O_RDONLY);
  //if (fd < 0){
  //  perror("open error\n");
  //  return 1;
  //}
  //char buffer[1024] = {0};
  //ssize_t s = read(fd, buffer, 1023);
  //if (s > 0)
  //{
  //  buffer[s] = 0;
  //  printf("%s\n", buffer);
  //}
  //close(fd);
  
  //int fd0 = open("./log1.txt", O_CREAT | O_WRONLY, 0644);
  //int fd1 = open("./log1.txt", O_CREAT | O_WRONLY, 0644);
  //int fd2 = open("./log1.txt", O_CREAT | O_WRONLY, 0644);
  //int fd3 = open("./log1.txt", O_CREAT | O_WRONLY, 0644);
  //int fd4 = open("./log1.txt", O_CREAT | O_WRONLY, 0644);
  //int fd5 = open("./log1.txt", O_CREAT | O_WRONLY, 0644);
  //printf("%d, %d, %d, %d, %d, %d\n", fd0, fd1, fd2, fd3, fd4, fd5);
  //close(fd0);
  //close(fd1);
  //close(fd2);
  //close(fd3);
  //close(fd4);
  //close(fd5);
  //return 0;
  
  const char* msg = "hello world\n";
  write(1, msg, strlen(msg));
  write(2, msg, strlen(msg));
  return 1;
}
















//int main()
//{
//  //写文件 w刷新写入 a追加写入
//  FILE* fp = fopen("log.txt", "w");
//  if (fp == NULL)
//  {
//    perror("fopen");
//    return 1;
//  }
//  const char* msg = "hello world\n";
//  fputs(msg, fp);
//  return 0;
//  
//  //读文件
//  //FILE* fp = fopen("log.txt", "r");
//  //if (fp == NULL){
//  //  perror("fopen failed");
//  //  return 1;
//  //}
//  //char buffer[64] = {0};
//  //while (fgets(buffer, 64, fp) != NULL);
//  //printf("%s", buffer);
//  //if (!feof(fp))
//  //  printf("file close failed\n");
//  //else 
//  //  printf("file close successs\n");
//  //return 0;
//
//  const char* msg = "hello world\n";
//  fputs(msg, stdout); //将数据重定向到文件中
//  return 0;
//}
