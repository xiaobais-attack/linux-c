ass Solution {
public:
    int reverse(int x) {
        long long flag = 1;
        long long res = 0;
        if (x < 0) flag = -1;
        x = abs(x);
        vector<long long> v;
        while (x){
            v.push_back(x % 10);
            x /= 10;
        }
        for (auto ele : v){
            if (INT_MAX / 10 < res) return 0;
            if (INT_MIN / 10 > res) return 0;
            res = res * 10 + ele;
        }
        return res * flag;

    }
};
