#pragma once
#include "my_rbtree.h"

namespace clx 
{
  template<class K, class V>
  class my_map
  {
    public:
    struct MapKeyOfT{
      const K& operator()(const pair<K, V>& kv) {return kv.first;}
    };
      typedef typename RBTree<K, pair<K, V>, MapKeyOfT>::iterator iterator;
      iterator begin() { return _t.begin(); }
      iterator end() { return _t.end(); }
      iterator find(const K& k) { return _t.find(k);}
      pair<iterator, bool> insert(const pair<K, V>& kv) {return _t.insert(kv);};
      V& operator[](const K& key)
      {
        pair<iterator, bool> ret = _t.insert(make_pair(key, V()));
        return ret.first->second;
      }
      void swap(my_map& m) {swap(_t, m._t);}
    private:
    RBTree<K, pair<K, V>, MapKeyOfT> _t;
  };

  void my_map_test()
  {
    my_map<string, string> m;
    m.insert(make_pair("left", "左边"));
    m.insert(make_pair("right", "右边"));
    m.insert(make_pair("map", "地图"));
    m["map"] = "1";
    my_map<string, string> mm;
    mm.insert(make_pair("clx", "no1"));
    swap(m, mm);
    my_map<string, string>::iterator it = m.begin();
    while (it != m.end())
    {
      cout << it->first << ":" << it->second << endl;
      ++it;
    }
    cout << endl;
    my_map<string, string> copy2;
    my_map<string, string> copy1;
    copy1  = copy2= m;
    for (auto e : copy2)
    {
      cout << e.first << ":" << e.second << endl;
    }
    for (auto e : copy1)
    {
      cout << e.first << ":" << e.second << endl;
    }
    //cout << endl;

    //auto ret = m.find("left");
    //cout << ret->first << (*ret).second << endl;
  }
}


