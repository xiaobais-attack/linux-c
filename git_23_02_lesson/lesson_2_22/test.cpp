#include <iostream>
using namespace std;

struct A{
  long a1 = 0;
  short a2 = 0;
  int a3 = 0;
  int* a4 = 0;
};


struct B{
  short a1;
  A a2;
};

struct C{
  short a1;
  char a2;
  short a3;
};
int main(){
  A a;
  B b;
  C c;
  cout << sizeof(a) << endl;
  cout << sizeof(b) << endl;
  cout << sizeof(long) << endl;
  cout << sizeof(c)<< endl;
  return 0;
}
