#include "comm.h"

int main()
{
  key_t key = ftok(PATH_NAME, PROJ_ID);
  if (key < 0){
    perror("ftok error\n");
    exit(1);
  }

  int shmid = shmget(key, SIZE, IPC_CREAT | IPC_EXCL | 0666);
  if (shmid < 0){
    perror("shmget error\n");
    exit(2);
  }

  char* mem = (char*)shmat(shmid , NULL, 0);
  printf("attatchs shm success\n");
  sleep(5);
  //业务逻辑
  while (1){
    sleep(1);
    printf("%s\n", mem);
  }

  shmdt(mem);;
  printf("detaches shm success\n");
  sleep(5);

  printf("key = %u, shmget = %d\n", key, shmid);
  sleep(10);

  shmctl(shmid, IPC_RMID, NULL);
  
  return 0;
}
