#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>


int main()
{
  //C语言函数
  printf("hello printf\n");
  fprintf(stdout, "hello fprintf\n");
  const char* msg = "hello write\n";
  write(1, msg, strlen(msg));
  sleep(10);
  return 0; 
}



//int main()
//{
//  close(0);
//  umask(0);
//  //int fd = open("log.txt", O_WRONLY|O_CREAT, 0666);
//  int fd = open("log.txt", O_RDONLY);
//  if (fd < 0)
//  {
//    perror("open");
//    return 1;
//  }
//
//  printf("hello printf\n");
//  fprintf(stdout, "hello fprintf\n");
//  fputs("hello fputs %d %d %f %c\n", stdout);
//
//  char buffer[100];
//  fgets(buffer, 100, stdin);
//  printf("%s\n", buffer);
//  
//  fflush(stdout);
//
//
//  close(fd);
//  return 0;
//}
