tion for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    // 1 2 3 1
    pair<ListNode*, ListNode*> reverseK(ListNode* head, ListNode* end) {
        ListNode* prev = nullptr, *cur = head;
        while (prev != end) {
            ListNode* next = cur->next;
            cur->next = prev;
            prev = cur;
            cur = next;
        }
        return {prev, head};
    }

    void printList(ListNode* head) {
        while (head) {
            cout << head->val << " ";
            head = head->next;
        }
        cout << endl;
    }
    
    ListNode* reverseKGroup(ListNode* head, int k) {
        ListNode* newHead = new ListNode(-1);
        newHead->next = head;
        ListNode *prev = newHead, *next = newHead, *cur = newHead;
        while (cur) {
            prev = cur;
            for (int i = 0; i < k; i++) {
                if (cur) cur = cur->next;
            }
            if (cur == nullptr) break; 
            next = cur->next;
            auto pa = reverseK(prev->next, cur);
            prev->next = pa.first;
            (pa.second)->next = next;
            cur = pa.second;
            printList(newHead);
            cout << cur->val << endl;
        }
        return newHead->next;
    }
};
