class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        unordered_map<char, int> book;
        int result = 0;
        int begin = -1;
        for (int i = 0; i < s.size(); i++) {
            if (book.find(s[i]) == book.end()) {
                book[s[i]] = i;
            } else {
                if (book[s[i]] >= begin) 
                    begin = book[s[i]];
                book[s[i]] = i;
            }
            result = max(result, i - begin);
        }
        return result;
    }
};
