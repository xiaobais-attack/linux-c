#include <iostream>
#include <string.h>
#include <assert.h>
using namespace std;

namespace clx
{
	class string
	{
	public:
		typedef char* iterator;
		typedef const char* const_iterator;

		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}

		const_iterator begin() const
		{
			return _str;
		}

		const_iterator end() const
		{
			return _str + _size;
		}

		void Swap(string& s)
		{
			::swap(_str, s._str);
			::swap(_capacity, s._capacity);
			::swap(_size, s._size);
		}

		string(const char* s = "")
			: _str(new char[strlen(s) + 1])
			, _size(0)
			, _capacity(0)
		{
			_size = _capacity = strlen(s);
			strcpy(_str, s);
		}

		string(const string& s)
			:_str(nullptr)
			, _size(0)
      , _capacity(0)
		{
			string tmp(s._str);
			Swap(tmp);
		}

		string& operator=(string s)
		{
			Swap(s);
			return *this;
		}

		~string()
		{
			delete[] _str;
			_capacity = _size = 0;
			_str = nullptr;
		}

		char* c_str()
		{
			return _str;
		}

		char* c_str() const
		{
			return _str;
		}

		char& operator[](size_t pos)
		{
			return _str[pos];
		}

		size_t size()
		{
			return _size;
		}

		//开空间
		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				_capacity = n;
			}
			
		}

		//开空间并且初识化
		void resize(size_t n, char val = '\0')
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				for (size_t i = _size; i < n; i++)
				{
					_str[i] = val;
				}
				_str[n] = '\0';
			}
			else if (n < _capacity && n > _size)
			{
				for (size_t i = _size; i < n; i++)
				{
					_str[i] = val;
				}
				_str[n] = '\0';
			}
			else
			{
				_size = n;
				_str[_size] = '\0';
			}
		}

		void push_back(char ch)
		{
			if (_size == _capacity)
			{
				reserve(_capacity * 2);
			}
			_str[_size] = ch;
			_str[_size + 1] = '\0';
			_size++;

		}

		void append(const char* str)
		{
			size_t len = _size + strlen(str);
			if (len > _capacity)
			{
				reserve(len);
			}

			strcpy(_str + _size, str);
			_size = len;
			
		
		}

		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}

		string& operator+=(const char* str)
		{
			append(str);
			return *this;
		}

		string& insert(size_t pos, const char* str)
		{
			assert(pos <= _size);
			int len = strlen(str);
			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}
			for (size_t i = _size; i >= pos && i <= _size; i--)
			{
				_str[i + len] = _str[i];
			}
			for (int i = 0; i < len; i++)
			{
				_str[pos + i] = *(str + i);
			}
			_str[_size + len] = '\0';
			_size = _size + len;
			return *this;
			
		}
		
		string& insert(size_t pos, char ch)
		{
			assert(pos <= _size);
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : 2 * _capacity);
			}
			for (size_t i = _size; i >= pos && i <= _size; i--)
			{
				_str[i + 1] = _str[i];
			}
			_str[pos] = ch;
			_size++;
			return *this;
		}

		string& erase(size_t pos, size_t len = npos)
		{
			assert(pos < _size);
			if (pos + len >= _size)
			{
				_size = pos;
				_str[pos] = '\0';
			}
			else
			{
				/*for (int i = 0; i < _size + 1 - pos - len; i++)
				{
					*(_str + pos + i) = *(_str + pos + len + i);
				}*/
				strcpy(_str + pos, _str + pos + len);
				_size -= len;
			}
			return *this;
		}

		size_t find(char ch, size_t pos = 0)
		{
			assert(pos < _size);
			for (size_t i = pos; i < _size; i++)
			{
				if (_str[i] == ch)
				{
					return i;
				}
			}
			return npos;
		}

		size_t find(const char* str, size_t pos = 0)
		{
			assert(pos < _size);
			const char* ret = strstr(_str + pos, str);
			if (ret)
			{
				return ret - _str;
			}
			else
			{
				return npos;
			}
		}
		
	private:
		char* _str;
		size_t _size;
		size_t _capacity;
		static size_t npos;
	};

	size_t string::npos = -1;
	bool operator==(const string& s1, const string& s2)
	{
		return !(strcmp(s1.c_str(), s2.c_str()));
	}
	bool operator<(const string& s1, const string& s2)
	{
		return strcmp(s1.c_str(), s2.c_str()) < 0;
	}
	bool operator>(const string& s1, const string& s2)
	{
		return strcmp(s1.c_str(), s2.c_str()) > 0;
	}
	bool operator<=(const string& s1, const string& s2)
	{
		return s1 < s2 || s1 == s2;
	}
	bool operator>=(const string& s1, const string& s2)
	{
		return s1 > s2 || s1 == s2;
	}
	bool operator!=(const string& s1, const string& s2)
	{
		return !(s1 == s2);
	}
	ostream& operator<<(ostream& out, const string s)
	{
		for (auto ch : s)
		{
			out << ch;
		}
		return out;
	}




	void string_test1()
	{
		string str1("hello world");
		string str2(str1);
		string str3("hello Linux");
		str2 = str3;
		cout << str1.c_str() << endl;
		cout << str2.c_str() << endl;
		cout << str3.c_str() << endl;
	}

	void string_test2()
	{
		string str1("hello world");
		for (size_t i = 0; i < str1.size(); i++)
		{
			cout << str1[i] << " ";
		}
		cout << endl;
		
	}

	void string_test3()
	{
		string s1("hello world");
		string::iterator it = s1.begin();
		while (it != s1.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;

		for (auto ch : s1)
		{
			cout << ch << " ";
		}
		cout << endl;
	}

	void string_test4()
	{
		string s1("hello world");
		s1 += " hello Linux  ";
		s1.resize(60);
		cout << s1.c_str() << endl;
		
	}

	void string_test5()
	{
		string s1("hello world");
		s1.resize(15, 'x');
		cout << s1.c_str() << endl;
		s1.insert(0, 'h');
		cout << s1.c_str() << endl;
		s1.insert(0, "Linux");
		cout << s1.c_str() << endl;
		s1.erase(1, 2);
		cout << s1.c_str() << endl;

	}

	void string_test6()
	{
		string s1("hello world");
		cout << s1.find('o') << endl;
		cout << s1.find("orl") << endl;

		cout << s1 << endl;
		s1.resize(20, 'x');
		cout << s1 << endl;
	}
}




int main()
{
	//clx::string_test1();
	//clx::string_test2();
	//clx::string_test3();
	/*clx::string_test4();*/
	/*clx::string_test5();*/

	clx::string_test6();

	string s1 = "hello world";

	return 0;
}


