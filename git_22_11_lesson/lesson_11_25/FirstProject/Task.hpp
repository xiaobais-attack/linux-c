#pragma once
#include <iostream>
#include "Protocol.hpp"

class Task
{
public:
  Task(int sock)
      : _sock(sock){};
  ~Task(){};
  void ProcessOn() { _handler(_sock); }

private:
  int _sock;
  CallBack _handler;
};
