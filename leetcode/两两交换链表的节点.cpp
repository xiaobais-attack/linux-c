/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        ListNode* newHead = new ListNode(0, head), *prev = newHead, *node1 = head;
        while (node1 && node1->next) {
            ListNode* node2 = node1->next, *next = node2->next;
            prev->next = node2;
            node2->next = node1;
            node1->next = next;
            prev = node1;
            node1 = next;
        }
        return newHead->next;

        
    }
};
