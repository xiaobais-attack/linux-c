#pragma once

#include <iostream>
#include <queue>
#include <string>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

namespace ns_thread_pool
{
    const int default_thread_num = 3;
    template <class T>
    class ThreadPool
    {
    private:
        std::queue<T> _task_queue;
        int _thread_num;
        pthread_mutex_t _mtx;
        pthread_cond_t _cond;
        static ThreadPool<T> *_ins;

    private:
        ThreadPool(int thread_num = default_thread_num)
            : _thread_num(thread_num)
        {
            pthread_mutex_init(&_mtx, nullptr);
            pthread_cond_init(&_cond, nullptr);
        }
        ThreadPool(const ThreadPool<T> &tp) = delete;
        ThreadPool &operator=(const ThreadPool<T> &tp) = delete;

    public:
        
        ~ThreadPool()
        {
            pthread_mutex_destroy(&_mtx);
            pthread_cond_destroy(&_cond);
        }

    public:
        static ThreadPool<T>* Getinstance()
        {
            static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
            if (_ins == nullptr)
            {
                pthread_mutex_lock(&lock);
                if (_ins == nullptr)
                {
                    _ins = new ThreadPool<T>();
                    _ins->InitThreadPool();
                    std::cout << "创建单例" << std::endl;
                }
            }
            pthread_mutex_unlock(&lock);
            return _ins;
        }
        void Lock()
        {
            pthread_mutex_lock(&_mtx);
        }
        void Unlock()
        {
            pthread_mutex_unlock(&_mtx);
        }
        void Wait()
        {
            pthread_cond_wait(&_cond, &_mtx);
        }

        void Wakeup()
        {
            pthread_cond_signal(&_cond);
        }
        bool is_empty()
        {
            return _task_queue.empty();
        }
        static void *thread_run(void *args)
        {
            pthread_detach(pthread_self());
            ThreadPool<T> *tp = (ThreadPool<T> *)args;
            while (true)
            {
                int data = 0;
                tp->Pop(&data);
                std::cout << "子线程处理了一个值 " << data << std::endl;
                sleep(1);
            }
        }

        void InitThreadPool()
        {
            pthread_t tid;
            for (int i = 0; i < default_thread_num; i++)
            {
                pthread_create(&tid, nullptr, thread_run, (void *)this);
            }
        }

    public:
        void Push(const T &in)
        {
            _task_queue.push(in);
            Wakeup();
        }
        void Pop(T *out)
        {
            Lock();
            while (is_empty())
            {
                Wait();
            }
            *out = _task_queue.front();
            _task_queue.pop();
            Unlock();
        }
    };

    template<class T>
    ThreadPool<T>* ThreadPool<T>::_ins = nullptr;
}
