class Solution {
public:
    vector<int> grayCode(int n) {
        vector<int> res = {0, 1};
        for (int i = 1; i < n; i++) {
            vector<int> copy(res);
            reverse(copy.begin(), copy.end());
            int add_num = pow(2, i);
            for (auto& ele : copy) {
                ele += add_num;
            }
            res.insert(res.end(), copy.begin(), copy.end());
        }
        return res;
    }
}
