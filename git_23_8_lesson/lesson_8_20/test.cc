#include "util.hpp"


void UtilTest1() {
  time_t time_stamp = clx_log::util::Date::now();
  struct tm time;
  localtime_r(&time_stamp, &time);
  char time_str[128];
  sprintf(time_str, "%d-%d-%d", time.tm_year + 1900, time.tm_mon + 1, time.tm_mday);
  std::cout << time_str << std::endl;

  std::cout << clx_log::util::File::exists("./a/b/c/d") << std::endl;
  clx_log::util::File::createDirectory("./a/b/c/d");
  std::cout << clx_log::util::File::exists("./a/b/c/d") << std::endl;
  std::cout << clx_log::util::File::path("./a/b/c/d") << std::endl;
}

int main() {
  UtilTest1();

}
