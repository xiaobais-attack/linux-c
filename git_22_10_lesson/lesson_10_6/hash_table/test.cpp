#include "hash_table.h"

int main()
{
  HashTable<int, int> ht;
  int arr[] = {2, 12, 22, 32, 42, 52, 62};
  for (auto e : arr)
  {
    ht.insert(make_pair(e, e));
    cout << e << endl;
  }
  ht.insert(make_pair(72, 72));
  return 0;
}
