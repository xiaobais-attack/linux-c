class Solution {
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k) {
        priority_queue<pair<int, int>> q;
        vector<int> result;

        for (int i = 0; i < k && i < nums.size(); i++) {
            q.push(make_pair(nums[i], i));
        }

        if (k > nums.size()) return {q.top().first};
        else result.push_back(q.top().first);

        for (int i = k; i < nums.size(); i++) {
            q.push(make_pair(nums[i], i));
            while (q.top().second <= i - k) {
                q.pop();
            }
            result.push_back(q.top().first);
        }
        return result;

    }
};
