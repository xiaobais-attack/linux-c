#pragma once
#include "LinkHash.h"

namespace clx
{
  template<class K, class V>
  class my_unordered_set
  {
    public:
      struct MapKeyOfT
      {
        K& operator()(const pair<K, V>& kv){
          return kv.first;
        }
      };
      //iterator begin();
      //iterator end;
      bool insert(const pair<K, V>& kv);
      bool erase(const K& key);
      //Find(const K& key);

    private:
      LinkHash::HashTable<K, pair<K, V>, MapKeyOfT, LinkHash::Hash<K>> _ht;
 
  };
}




