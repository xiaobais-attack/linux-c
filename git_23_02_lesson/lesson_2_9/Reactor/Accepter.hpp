#pragma once 
#include "Reactor.hpp" //typedef int(*callback_t)(Event *ev); 为了使用这个类型
#include "Sock.hpp"
#include "Util.hpp"
#include "Service.hpp"

int Accepter(Event* evp){
    std::cout << "有新的连接到来了,就绪的sock是: " << evp->sock << std::endl;
    while (true){
        // struct sockaddr_in peer;
        // socklen_t len = sizeof(peer);
        // int sock = accept(evp->sock, (struct sockaddr*)&peer, &len);

        int sock = Sock::Accept(evp->sock);
        if (sock < 0){
            std::cout << "Accept Done" << std::endl;
            break;
        }
        SetNonBlock(sock);
        std::cout << "Accept success:" << sock << std::endl;
        // 获取连接成功了
        Event *other_evp = new Event();
        other_evp->sock = sock;
        other_evp->R = evp->R;
        other_evp->RegisterCallback(Recver, Sender, Errorer);
        evp->R->InsertEvent(other_evp, EPOLLIN | EPOLLET);
    }
}