@[TOC]
# 1.0 数据库基本概念
想要理解数据库的概念，首先要了解数据库为何存在，它存在的价值是什么？
在之前的学习中，我们通常用文件来存储数据， 但是文件存储数据拥有以下几个缺点。
1.文件的安全性无法保证
2.文件不利于数据查询和管理
3.文件不利于存储海量数据
4.文件在程序中控制不方便
为了解决上述问题，专家们设计出更加利于管理数据的应用层软件--数据库，使用它可以更加有效快捷的管理数据。所以数据库是需要我们自己安装，其底层一定是直接或者间接的访问OS的文件接口
![在这里插入图片描述](https://img-blog.csdnimg.cn/2b2709ef9cc5493698dd8970aa22ca76.png)
**查看数据库服务器链接状况**
指令：netstat -nltp 查看是否链接上MySQL服务器
![在这里插入图片描述](https://img-blog.csdnimg.cn/29b1d9a0de874c3fb32224001927bf77.png)


##  1.1 主流数据库
SQL Sever： 微软的产品，.Net程序员的最爱，中大型项目。
**Oracle (部分银行使用)： 甲骨文产品，适合大型项目，复杂的业务逻辑，并发一般来说不如MySQL。**
**MySQL (主流)：世界上最受欢迎的数据库，属于甲骨文，并发性好，不适合做复杂的业务。主要用在电商，SNS，论
坛。对简单的SQL处理效果好。**
PostgreSQL :加州大学伯克利分校计算机系开发的关系型数据库，不管是私用，商用，还是学术研究使用，可
以免费使用，修改和分发。
SQLite： 是一款轻型的数据库，是遵守ACID的关系型数据库管理系统，它包含在一个相对小的C库中。它的设
计目标是嵌入式的，而且目前已经在很多嵌入式产品中使用了它，它占用资源非常的低，在嵌入式设备中，可
能只需要几百K的内存就够了。
H2： 是一个用Java开发的嵌入式数据库，它本身只是一个类库，可以直接嵌入到应用项目中。



# 2.0 MySQL架构
MySQL 是一个**可移植的数据库**，几乎能在当前所有的操作系统上运行，如 Unix/Linux、Windows、Mac 和Solaris。各种系统在底层实现方面各有不同，但是 MySQL 基本上能保证在各个平台上的物理体系结构的一致性

> 可移植性在代码层面理解：例如一段程序想要在三十二位和六十四位机器都可以跑，则需要通过条件编译将三十二位和六十四位有区别的部分代码使用条件编译的手段穷举出来，让不同机器得到自己需要的代码，将无用代码去掉


![在这里插入图片描述](https://img-blog.csdnimg.cn/0afa794ea5ff45de9cbbc4ca31bb08e5.png)
**第一层：Connection Pool 链接池，MySQL服务器采用多线程技术，服务器创建好线程等待客户端网络链接
第二层：词法语法分析，对用户传来的SQL语句进行分析，然后去调用指定接口处理数据
第三层： 存储引擎，对磁盘中的特定文件数据进行访问操作，管理数据**

## 2.1 MySQL存储引擎
 **MySQL的核心是插件式存储引擎，支持多种存储引擎
 存储引擎是数据库管理系统如何存储数据，如何位存储的数据建立索引和如何更新、查询数据等技术的实现方法**
>我们可以通过不同的接口调用不同的存储引擎帮我们存储数据。其设计理念和Linux文件管理类似，我们只需要将每个存储引擎中封装一系列函数指针，通过接口调用不同存储引擎就可以使用不同的形势来存储数据 

**查看存储引擎**
**指令: show engines(+\G选项可以改变打印方式)**
![在这里插入图片描述](https://img-blog.csdnimg.cn/187a04507a4546a4ae409542525c2da0.png)


## 2.2 SQL语句分类
**DDL【data definition language】 数据定义语言，用来维护存储数据的结构  代表指令：create, drop, alter
DML【data manipulation language】 数据操纵语言，用来对数据进行操作   代表指令： insert，delete，update
DML中又单独分了一个DQL，数据查询语言，代表指令： select
DCL【Data Control Language】 数据控制语言，主要负责权限管理和事务   代表指令： grant，revoke，commit**

# 3.0 MySQL基本使用
## 3.1 数据库操作汇总

```cpp
vim /etc/my.cnf                         //查看数据库配置文件
mysql -h +ip + -p + port + -uroot -p    //登录MySQL数据库
show create database database_name;     //查看数据库创建信息
show databases;                         //打印MySQL中的数据库
show tables;                            //查看数据库中的表
show create database database_name      //查看数据库的创建信息
show create table table_name            //查看表创建信息
show engines;                           //查看MySQL支持的存储引擎
show processlist;                       //查看进程链接(有多少个进程链接我们的MySQL)
desc table_name                         //查看表结构
create database + database_name;        //创建数据库
use database_name                       //使用数据库（类似Linux中的cd）
select * from person where name='a';



```

## 3.2 数据库操作理解
**查看数据库配置文件**
**指令: vim /etc/my.cnf**
![在这里插入图片描述](https://img-blog.csdnimg.cn/c9fab34e96224abcbce10d94c8efc350.png)
**登录数据库**
**指令： mysql -h +ip + -p + port + -uroot -p**
```cpp
[clx@VM-20-6-centos mysql]$ mysql -uroot -p   //登录本机mysql可以简化
[clx@VM-20-6-centos mysql]$ mysql -h 127.0.0.1 -P 3306 -u root -p
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/431cdffe072e481d95264d0df4f49a2f.png)


**查看MySQL中的库**
**指令: show databases**
![在这里插入图片描述](https://img-blog.csdnimg.cn/a49d4345d16346ddb734e82679db6a52.png)
可以看到数据库中中的库与datadir目录下的各个目录一一对应
**添加MySQL中的库**
**指令： create database + database_name ;**

```cpp
MariaDB [(none)]> create database clx_database;   //添加库
Query OK, 1 row affected (0.00 sec)
MariaDB [(none)]> show databases;                 //查看库
+--------------------+
| Database           |
+--------------------+
| information_schema |
| clx_database       |
| demo_db            |
| mysql              |
| performance_schema |
| test               |
+--------------------+
6 rows in set (0.00 sec)
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/d6d8dcc57d7e45469bc08c08f3bdffb5.png)
可以看到我们在MySQL中创建了一个名为clx_database库，在配置文件中的datadir 目录下就创建了一个clx_database目录.**所以在MySQL中建立一个库的本质，就是在datadir 目录下创建一个目录**

**选择数据库**
**指令: use + clx_database（数据库名称）;**

**在MySQL库中创建表**

```cpp
MariaDB [(none)]> use clx_database
Database changed
MariaDB [clx_database]> create table if not exists `hello` (  //若hello表不存在则进行创建
    -> name varchar(16) not null,                         //第一个参数 名字:name 类型:varchar(16)
    -> age int not null                                   //第二个参数 名字:age  类型:int  
    -> );
Query OK, 0 rows affected (0.01 sec)                      //表示创建成功

```
**查看MySQL库中的表**
指令: show tables;
![在这里插入图片描述](https://img-blog.csdnimg.cn/317a21de5ce74935989782c503a74615.png)
	**所谓的创建数据库表，本质就是在特定的目录下创建特定的文件！(一张表可能对应多个文件，和存储引擎有关)**
	![在这里插入图片描述](https://img-blog.csdnimg.cn/579dd4da6c8946c296f5ca0ff1ab64b9.png)
**服务器找到对应的库和表本质就是，找到对应的目录以及文件**

**查看表结构**
指令：desc + 表名称；
 ![在这里插入图片描述](https://img-blog.csdnimg.cn/4bcba5f55e4343c99123eee4caee7bcd.png)


**向表中插入数据**
指令：insert hello(name age) values ('clx', 19);
![在这里插入图片描述](https://img-blog.csdnimg.cn/0a63255b39bb43709a504b52f58f0225.png)

**将表中数据全部打印**
指令： select * from + 表名称；
**删除MySQL的库**

指令: drop database + 数据库名; 

# 4.0 数据库操作详解

## 4.1创建数据库
**1.创建名为 clx_database 的数据库**

```cpp
create database clx_database
```

**说明：当我们创建数据库没有指定字符集和校验规则时，我们希望系统使用默认字符集：utf8，校验规则是：utf8_general_ ci(可以支持中文存储), 当然我们也可以去系统的配置文件中修改默认字符集和校验规则**

**2.创建一个使用utf8字符集，utf8_general_ci为校验规则的 clx_database 数据库**
```cpp
create database clx_database charset=utf8 collate utf8_general_ci;
```

## 4.2 字符集和校验规则
**字符集：每种字符集代表一种将字符转换成二进制序列方式
校对规则：每种校对规则代表一种将二进制序列转化成字符的方式**

> 字符集和校对规则一一对应，应该成对使用，若使用不匹配的字符集和校对规则可能会出现乱码，数据错乱等现象

### 4.2.1 查看系统默认字符集以及校验操作

```cpp
show variables like 'character_set_database';
show variables like 'collation_database';
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/47f9d83e120e419cbd22b6f75c171fb4.png)

> 我已经将我的MySQL默认设置调整好了，若不清楚如何改默认设置可以看一下 FatePuffer大佬的这篇博客  [配置MySQL数据库](https://blog.csdn.net/qq_42517220/article/details/101368838?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522166988342016800182789502%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=166988342016800182789502&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v31_ecpm-2-101368838-null-null.142%5Ev67%5Econtrol,201%5Ev3%5Eadd_ask,213%5Ev2%5Et3_control2&utm_term=centos7%E5%AE%89%E8%A3%85mariadb%E6%95%B0%E6%8D%AE%E5%BA%93%E6%80%8E%E4%B9%88%E4%BF%AE%E6%94%B9%E9%BB%98%E8%AE%A4%E5%AD%97%E7%AC%A6%E9%9B%86%E5%92%8C%E7%BC%96%E7%A0%81%E8%A7%84%E5%88%99&spm=1018.2226.3001.4187)
### 4.2.2 查看MySQL支持的字符集和校验规则

```cpp
show charset    //查看支持的字符集
show collation  //查看支持的校验规则
```
### 4.2.3 字符集和校验规则不匹配对数据库的影响
**创建两个数据库，两者的字符集均为utf8, 前者的校验规则使用 utf8_general_ci【不区分大小写】，后者的校验规则使用utf8_bin【区分大小写】**

```cpp
MariaDB [(none)]> create database test1 charset=utf8 collate utf8_general_ci; 
Query OK, 1 row affected (0.00 sec)
MariaDB [(none)]> create database test2 charset=utf8 collate utf8_bin;
Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> show create database test1;
+----------+----------------------------------------------------------------+
| Database | Create Database                                                |
+----------+----------------------------------------------------------------+
| test1    | CREATE DATABASE `test1` /*!40100 DEFAULT CHARACTER SET utf8 */ | 
+----------+----------------------------------------------------------------+
1 row in set (0.00 sec)                    
MariaDB [(none)]> show create database test2;
+----------+---------------------------------------------------------------------------------+
| Database | Create Database                                                                 |
+----------+---------------------------------------------------------------------------------+
| test2    | CREATE DATABASE `test2` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */ |
+----------+---------------------------------------------------------------------------------+
1 row in set (0.00 sec)

```

> 注意：当字符集和校准规则是匹配的时候，使用show create database database_name; 查看库信息时，校准规则会被省略，若不匹配则会打印出来

```cpp
MariaDB [test1]> create table person(name varchar(20)); //在test1数据库中创建person表
Query OK, 0 rows affected (0.01 sec)

MariaDB [test1]> insert into person values('a');
Query OK, 1 row affected (0.01 sec)

MariaDB [test1]> insert into person values('b');
Query OK, 1 row affected (0.01 sec)

MariaDB [test1]> insert into person values('A');
Query OK, 1 row affected (0.00 sec)

MariaDB [test1]> insert into person values('B');
Query OK, 1 row affected (0.00 sec)

MariaDB [test1]> select * from person ;
+------+
| name |
+------+
| a    |
| b    |
| A    |
| B    |
+------+
4 rows in set (0.00 sec)

MariaDB [test1]> select * from person where name='a';  //可以发现并没有区分大小写
+------+
| name |
+------+
| a    |
| A    |
+------+
2 rows in set (0.00 sec)
MariaDB [test1]> use test2
Database changed
MariaDB [test2]> create table person(name varchar(20));//在test2目录中创建person表
Query OK, 0 rows affected (0.01 sec)

MariaDB [test2]> insert into person values('a');
Query OK, 1 row affected (0.00 sec)

MariaDB [test2]> insert into person values('b');
Query OK, 1 row affected (0.00 sec)

MariaDB [test2]> insert into person values('A');
Query OK, 1 row affected (0.01 sec)

MariaDB [test2]> insert into person values('B');
Query OK, 1 row affected (0.00 sec)

MariaDB [test2]> select * from person
    -> ;
+------+
| name |
+------+
| a    |
| b    |
| A    |
| B    |
+------+
4 rows in set (0.01 sec)

MariaDB [test2]> select * from person where name='a';//发现区分大小写，只打印了一个数据
+------+
| name |
+------+
| a    |
+------+
1 row in set (0.00 sec)

MariaDB [test2]> 

```
**通过上述实验我们可以发现使用相同的字符集但是若校验规则不同会影响数据库的运行结果**

## 4.2修改数据库
**指令：alter database database_name  charset=gbk** 
**将指定数据库的字符集改成gbk**

```cpp
MariaDB [(none)]> show create database test1;
+----------+----------------------------------------------------------------+
| Database | Create Database                                                |
+----------+----------------------------------------------------------------+
| test1    | CREATE DATABASE `test1` /*!40100 DEFAULT CHARACTER SET utf8 */ | 
+----------+----------------------------------------------------------------+
1 row in set (0.00 sec)                    
MariaDB [test1]> alter database test1 charset=gbk;
Query OK, 1 row affected (0.00 sec)
MariaDB [test1]> show create database test1;
+----------+---------------------------------------------------------------+
| Database | Create Database                                               |
+----------+---------------------------------------------------------------+
| test1    | CREATE DATABASE `test1` /*!40100 DEFAULT CHARACTER SET gbk */ |
+----------+---------------------------------------------------------------+
1 row in set (0.00 sec)

```

## 4.3删除数据库
**指令：drop database database_name;**

```cpp
MariaDB [test1]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| demo_db            |
| mysql              |
| performance_schema |
| test1              |
| test2              |
+--------------------+
6 rows in set (0.00 sec)

MariaDB [test1]> drop database test1; drop database test2;
Query OK, 1 row affected (0.01 sec)

Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| demo_db            |
| mysql              |
| performance_schema |
+--------------------+
4 rows in set (0.00 sec)

```

## 4.4查看数据库链接
**指令：show processlist;** 
![在这里插入图片描述](https://img-blog.csdnimg.cn/2c422195c3584dba82e1de9eb4338355.png)

