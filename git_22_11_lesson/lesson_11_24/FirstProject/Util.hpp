#pragma once 
#include <string>
#include <sys/types.h>
#include <sys/socket.h>


class Util{
  public:
    static int ReadLine(int sock, std::string &out);
    static bool CutString(std::string &target, std::string& key_out, std::string& value_out, std::string sep);
};

int Util::ReadLine(int sock, std::string& out){
  char ch = 'c';
  while (ch != '\n'){
    ssize_t size = recv(sock, &ch, 1, 0);
    if (size > 0){
      if (ch == '\r'){ //进行特殊处理
        recv(sock ,&ch, 1, MSG_PEEK);
        if (ch == '\n'){
          recv(sock ,&ch, 1, 0);
        }
        else{
          ch = '\n';
        } 
      }
      //1.普通字符   2.\n
      out.push_back(ch);
    }
    else if (size == 0){
      return 0;
    }
    else{
      return -1;
    }
  }
  return out.size();
}

bool Util::CutString(std::string& target, std::string& key_out, std::string& value_out, std::string sep){
  size_t pos = target.find(sep);
  if (pos != std::string::npos){
    key_out = target.substr(0, pos);
    value_out = target.substr(pos + sep.size());
    return true;
 }
  return false;
}




















