#include <iostream>
#include <unistd.h>
#include <poll.h>

int main()
{
    struct pollfd rfds
    {
        0, POLLIN , 0
    };
    while (true)
    {

        int n = poll(&rfds, 1 ,1000); // 0非阻塞轮询 -1阻塞
        switch (n)
        {
        case 0:
            std::cout << "time out ..." << std::endl;
            break;
        case -1:
            std::cerr << "poll error"  << std::endl;
            break;
        default:
            std::cout << "有事件发生..." << std::endl;
            if (rfds.revents & POLLIN){
                std::cout << "读事件发生了" << std::endl;
                char buffer[128];
                ssize_t s = read(0, buffer, sizeof(buffer) - 1);
                if (s > 0){ std::cout << "有人说#" << buffer << std::endl; }
            }
            break;
        }
    }
    return 0;
}