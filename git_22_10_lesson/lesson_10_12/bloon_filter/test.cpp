#include "my_filter.h"
#define N 1000
#define X 10


void test1()
{
  bloon_filter<100, 10, BKDRHash, APHash, DJBHash> bf;
  vector<string> v = {"sort", "left", "right"};
  for (auto str : v)
  {
    bf.set(str);
    cout << endl;
  }
  for (auto& str: v)
  {
    cout << bf.test(str) << endl;
  }
  cout << bf.test("math") << endl;
  cout << endl;
}

void test2()
{
  bloon_filter<N, X, BKDRHash, APHash, DJBHash> bf;
  bitset<N * X> bs;
  vector<string> v1;
  vector<string> v2;
  vector<string> v3;
  for (int i = 0; i < N; i++)
  {
    string base = "https://gitee.com/bithange/class_code/blob/master/class";
    string end = base + to_string(1234 + i);
    v1.push_back(end);
  }

  for (int i = 0; i < N; i++)
  {
    string base = "https://gitee.com/bithange/class_code/blob/master/class";
    string end = base + to_string(5678 + i);
    v2.push_back(end);
  }

  
  for (int i = 0; i < N; i++)
  {
    string base = "https://fanyi.baidu.com/translate?aldtype=16047&query";
    string end = base + to_string(5678 + i);
    v3.push_back(end);
  }

  BKDRHash ap;
  for (auto str : v1)
  {
    bf.set(str);
    bs.set(ap(str) % (N*X));
  }

  size_t n1 = 0;
  size_t n3 = 0;
  for (auto str: v2)
  {
    if (bf.test(str))
      n1++;
    if (bs.test(ap(str) % (N*X)))
      n3++;
  }

  size_t n2 = 0;
  size_t n4 = 0;
  for (auto str : v3)
  {
    if (bf.test(str))
      n2++;
    if (bs.test(ap(str) % (N*X)))
      n4++;
  }


  cout << "布隆过滤器相似误判率为" << (double)n1 / N << endl;
  cout << "布隆过滤器非相似误判率为" << (double)n2 / N << endl;
  cout << "直接位图相似误判率为" << (double)n3 / N << endl;
  cout << "直接位图非相似误判率为" << (double)n4 / N << endl;
}

int main()
{
  test2();
  return 0;
}
