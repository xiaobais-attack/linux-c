#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

void print_pending(sigset_t* pending){
  for (int i = 1; i < 31; i++){
    if (sigismember(pending, i)){
      printf("1");
    }
    else {
      printf("0");
    }
  }
  printf("\n");
}

void handler(int signum)
{
  printf("%d singal delivered\n", signum);
}

int main()
{
  int count = 0;
  signal(2, handler);
  sigset_t block_set, oldset;
  sigset_t pending;
  sigemptyset(&pending);
  sigemptyset(&block_set);
  sigaddset(&block_set, 2);
  sigprocmask(SIG_SETMASK, &block_set, &oldset);
  while (1){
    if (++count == 20){
      sigprocmask(SIG_SETMASK, &oldset, NULL);
    }
    sigemptyset(&pending);
    sigpending(&pending);
    printf("count = %d  ", count);
    print_pending(&pending);
    sleep(1);
  }
}




//int main()
//{
//  sigset_t set;
//  sigset_t oldset;
//
//  sigemptyset(&set);
//  sigemptyset(&oldset);
//
//  sigaddset(&set, 2);
//  sigprocmask(SIG_SETMASK, &set, &oldset);
//
//  while (1){
//    sleep(1);
//    printf("I'm a process, my pid = %d\n", getpid());
//  }
//  return 0;
//}
