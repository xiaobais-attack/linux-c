class Solution {
public: 
    // a b c d e
    // a c e 
    // book[i][j] 
    // 0 0 0 0  
    // 0 1 0 0
    // 0 0 0 0 
    // 0 0 2 0
    // 0 0 0 3
    int longestCommonSubsequence(string text1, string text2) {
        int m = text1.size(), n = text2.size();
        vector<vector<int>> book(m + 1, vector<int>(n + 1, 0));

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                book[i][j] += max(book[i - 1][j], book[i][j - 1]);
                if (text1[i - 1] == text2[j - 1]) {
                    book[i][j] = book[i - 1][j - 1] + 1;
                } else {
                    book[i][j] = max(book[i - 1][j], book[i][j - 1]);
                }
            }
        }


        return book[m][n];
    }
};
