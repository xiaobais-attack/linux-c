class Solution(object):
    def threeSum(self, nums):
        result = list()
        nums.sort()
        for i in range(len(nums)):
            if i >= 1 and nums[i - 1] == nums[i]: continue
            target = -nums[i]
            begin = i + 1
            end = len(nums) - 1
            while begin < end :
                if nums[begin] + nums[end] > target :
                    end -= 1
                elif nums[begin] + nums[end] < target :
                    begin += 1
                else :
                    result.append([nums[i], nums[begin], nums[end]])
                    while begin < end and nums[begin] == nums[begin + 1] : begin += 1
                    while begin < end and nums[end] == nums[end - 1] : end -= 1
                    begin += 1
                    end -= 1

        return result;



