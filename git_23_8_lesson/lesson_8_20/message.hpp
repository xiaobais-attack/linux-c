/* 
    日志消息类, 用于进行日志中间信息的存储：
    1、 日志的输出时间        可用于过滤日志
    2、 日志的等级            用于进行日志的过滤分析
    3、 源文件名称  
    4、 源代码行号            用于定位出现错误的代码位置
    5、 线程ID                用于过滤出错的线程
    6、 日志主题消息
    7、 日志器名称            项目允许多日志器同时使用
*/ 

#ifndef __CLX_MESSAGE_H__
#define __CLX_MESSAGE_H__

#include "level.hpp"
#include "util.hpp"
#include <thread>

namespace clxlog {
    struct LogMsg {
        LogLevel::value _level;          // 日志等级
        time_t _ctime;                   // 日志产生的时间戳
        size_t _line;                    // 源文件行号
        std::string _file;               // 源文件名称
        std::string _logger;             // 日志器
        std::string _payload;            // 有效载荷，日志主题消息
        std::thread::id _tid;            // 线程ID

        LogMsg(LogLevel::value level, size_t line, const std::string file, const std::string logger, const std::string msg) 
            : _level(level), _ctime(util::Date::now()), _line(line), _file(file), _logger(logger), _payload(msg), _tid(std::this_thread::get_id()) {}
    };
}


#endif
