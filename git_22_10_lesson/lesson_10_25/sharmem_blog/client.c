#include "comm.h"

int main()
{
  key_t key = ftok(SHM_PATH, SHM_ID);
  if (key < 0){
    perror("ftok failed\n");
    exit(1);
  }
  int shmid = shmget(key, 4*1024, IPC_CREAT);
  char* my_shm = (char*)shmat(shmid, NULL, 0);
  printf("client process attaches success\n");
  for (int i = 0; i < 26; i++){
    sleep(1);
    char ch = 'a' + i;
    my_shm[i] = ch;
  }
  shmdt(my_shm);
  return 0;
}
