#include "thread_pool.h"
using namespace ns_thread_pool;


int main(){

    srand((long long)time(nullptr));
    ThreadPool<int>* tp = ThreadPool<int>::Getinstance();
    tp->InitThreadPool();
    


    while (true){
        int data = rand() % 20 + 1;
        tp->Push(data);
        std::cout << "主线程发布一个值 " << data << std::endl;
        sleep(1);
    }
    
}
