#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <string.h>
#include <algorithm>
using namespace std;

namespace clx 
{
  template<class T>
  class vector 
  { 
    public:
      typedef T* iterator;
      typedef const T* const_iterator;
    iterator begin()
    {
      return _start;
    }
    iterator end()
    {
      return _finish;
    }
    const_iterator begin() const 
    {
      return _start;
    }
    const_iterator end() const 
    {
      return _finish;
    }
    vector();
    vector(const vector<T>& v);
    vector<T>& operator=(const vector<T> v);
    ~vector();
    size_t size() const;
    size_t capacity() const;
    void reserve(size_t n);
    void resize(size_t n, T val = T());
    void push_back(const T& x);
    void pop_back();
    T& operator[](size_t n);
    void insert(iterator pos, const T& x);
    iterator& erase(iterator pos);


    
    private:
    iterator _start;
    iterator _finish;
    iterator _endofstorage;
  };
  template<class T>
  void print_vector(vector<T> v);
  void vector_test1();
  void vector_test2();
  void vector_test3();
  void vector_test4();
}
